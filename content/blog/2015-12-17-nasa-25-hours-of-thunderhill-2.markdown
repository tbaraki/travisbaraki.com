---
author: travisba
comments: true
date: 2015-12-17 02:21:30+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/video/nasa-25-hours-of-thunderhill-2/
slug: nasa-25-hours-of-thunderhill-2
title: 'Video: NASA 25 Hours of Thunderhill'
wordpress_id: 1223
categories:
- Video
---

With so many different cars, there were many different sounds to enjoy. The elevations changes really show up well on video. I imagine that this track drives like a rollercoaster.

<!-- more -->

This race marked quite a few "firsts" for me. Like I mentioned in the race [gallery](http://www.travisbaraki.com/photo/nasa-25-hours-of-thunderhill-gallery/), this was my first time shooting at night and in the rain. It was also my first time shooting with my new [tripod](http://amzn.to/1UBSztS) and [fluid head](http://amzn.to/1OxbVeR). I'm happy with some of the pans I got, especially considering most were at 200mm and following fast race cars. Still, many were spoiled by operator error. There is much room to improve.

Please enjoy nearly nine minutes of pure ambient sounds from Thunderhill.

[youtube id="RdqceUwyB4M" align="center" mode="lazyload-lightbox" autoplay="no" maxwidth="700" grow="yes"]
