---
author: travisba
date: 2019-02-09
draft: false
slug: indycar-test
title: "Indycar Test"
categories:
  - Photo
  - Video
---

It was awesome to see Indycar back at Laguna Seca. A few random thoughts/notes from the test:

- First time seeing the new chassis in person (I missed the last couple Sonama races). Great proportions and curves.
- I forgot how loud these cars are, especially the Honda. While the turbos definitely remove some of the harshness, these sound like a proper racecar should.
- The track was insanely green at first. Very sandy.
- Times on the green track started out in the 1:16s, eventually dropping into the 1:14s.
- A few guys were _on it_ immediately... Pagenaud, Rossi, Veach, and Chilton in particular.
- Marshall Pruett reported that drivers had gotten into the low to mid 1:12s by mid morning

{{< tweet 1093945560476741632 >}}

- It looked like cars were able to follow pretty well through turns 3-4-5-6. Hopefully that means we'll see some overtaking come Fall.
- By the end of the day, times were set in the low 11s ([racer.com](https://racer.com/2019/02/08/chilton-tops-rain-shortened-laguna-test/)). Will we approach the 1:07.722 Champ Car lap record with warmer weather, rubbered in track, and Firestone Reds? We'll see.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/90679994@N08/albums/72157705108416741" title="Indycar Test - Laguna Seca"><img src="https://farm8.staticflickr.com/7833/32096740777_24c317212a_b.jpg" width="1024" height="683" alt="Indycar Test - Laguna Seca"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

{{< youtube OdofcFdf0ok >}}
