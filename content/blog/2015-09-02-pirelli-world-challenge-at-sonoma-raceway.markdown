---
author: travisba
comments: true
date: 2015-09-02 00:51:59+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/video/pirelli-world-challenge-at-sonoma-raceway/
slug: pirelli-world-challenge-at-sonoma-raceway
title: 'Video: Pirelli World Challenge at Sonoma Raceway'
wordpress_id: 772
categories:
- Video
---

Renger van der Zande and Kevin Estre stole the show. I watched race one at home before driving up for race two and the [IndyCar race](http://www.travisbaraki.com/photo/indycar-gopro-grand-prix-of-sonoma/) on Sunday. Estre took race one with a forceful, but clean, pass at the bottom of the Carousel. Van der Zande responded in race two. A great start put him up front and he managed to hold off intense pressure from Estre from then on. Great to watch. Unfortunately, an Acura and a Bentley were caught up in turn two mayhem. It would have been great to see the full field coming down through the esses into the chicane. I can't wait to see them again at Laguna Seca in a couple weeks.

<!-- more -->


