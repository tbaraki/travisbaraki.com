---
author: travisba
comments: true
date: 2015-09-13 03:50:44+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/photo/pirelli-world-challenge-saturday/
slug: pirelli-world-challenge-saturday
title: 'Gallery: Pirelli World Challenge — Saturday'
wordpress_id: 887
categories:
- Photo
---

Title contenders Johnny O'Connell and Olivier Beretta qualified 5th and 12th, respectively. Beretta's qualifying was compromised when the session was red flagged to recover Chris Dyson's Bentley from the exit of turn 4. The stage is set for a dramatic finale.

<!-- more -->

[juicebox gallery_id="20"]
