---
author: travisba
comments: true
date: 2015-08-16 19:25:35+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/photo/rolex-monterey-motorsports-reunion/
slug: rolex-monterey-motorsports-reunion
title: 'Gallery: Rolex Monterey Motorsports Reunion'
wordpress_id: 692
categories:
- Photo
---

The Reunion is such a fantastic event. This has to be the finest collection of pedigreed historic racing cars outside of Goodwood. The [Pre-Reunion](http://www.travisbaraki.com/photo/monterey-pre-reunion/) was truly just a taste of what was in store. When they take to the track, it is definitely not a series of gentle parade laps. They race. Hard. I'm glad days like this exist; It makes me feel better about being born a few decades too late to enjoy these cars the first time around.

<!-- more -->

The FIA/IMSA GTP and GTO cars really speak to me. They come from an era of intense, and expensive, manufacturer involvement and rapid innovation. There are some very interesting designs. While they are terribly underrepresented in this gallery, the 1967-1984 Formula 1 cars were damn impressive. In their day, they were conquered by names like Hunt, Prost, Rosberg, Jones, Stewart, and Lauda. The pack was mostly powered by the venerable Ford/Cosworth DFV with a few 12-cylinder cars mixed in. What a sound. Video is forthcoming.

[juicebox gallery_id="16"]
