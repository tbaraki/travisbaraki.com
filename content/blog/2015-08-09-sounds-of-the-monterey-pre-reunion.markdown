---
author: travisba
comments: true
date: 2015-08-09 16:42:55+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/video/sounds-of-the-monterey-pre-reunion/
slug: sounds-of-the-monterey-pre-reunion
title: 'Video: Sounds of the Monterey Pre-Reunion'
wordpress_id: 680
categories:
- Video
---

Flat-6 howl? Check. Rotary scream? Check. V8 thunder? Check. I only wish I had more time. Cars were only on track for 20 minutes at a time so I rushed to capture what I could. I would love to have got them screaming through turn 1 as well as coming down through the corkscrew. I'll do my best next weekend at the ROLEX Monterey Motorsport Reunion proper. I am looking forward to seeing the 1968-1984 Formula 1 cars out on track.

<!-- more -->

Also check out the [photo gallery](http://www.travisbaraki.com/photo/monterey-pre-reunion/) from the weekend if you haven't already.


