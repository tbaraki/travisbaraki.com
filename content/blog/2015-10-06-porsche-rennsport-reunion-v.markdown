---
author: travisba
comments: true
date: 2015-10-06 02:25:58+00:00
draft: false
layout: post
link: http://www.travisbaraki.com/video/porsche-rennsport-reunion-v/
slug: porsche-rennsport-reunion-v
title: 'Video: Porsche Rennsport Reunion V'
wordpress_id: 1089
categories:
- Video
---

## Race Groups 5 & 6

This video features some of my all-time favorite cars. Turbocharged monsters like the 935 and 962 take to track with modern classics such as the RS Spyder and the sweet sounding 911 GT3 RSR. I loved seeing the handful of 993 RSRs out on track.

With names like Jeroen Bleekemolen, Cooper MacNeil, Hurley Haywood, and Marina Franchitti behind the wheel, you can be sure these were not parade laps. Cars were pushed hard and mistakes were made. One of the RS Spyders went off at turn 6 and Bruce Canepa clipped the tires going into the Corkscrew leaving a big black mark on his 962C. Heartbreaking, but less so than the cars sitting in a garage.

{{< youtube Eq8OBOHTic4 >}}

## Race Group 4

Porsche's 917 is instantly recognizable as a relic from a bygone era. Well over 200MPH down the Mulsanne at night, in the wet, on bias-ply tires? Pure madness. Also on track were some very cool, and equally iconic, 908 examples including a fantastic long-tail.

{{< youtube xlEZamxAMTA >}}

## Race Group 3

It was easy to pick out the 2.5 liter cars from the 2.0 liter cars. They would squirt away on exit or up the hill but would have the lighter, more nimble 2.0 cars right back on their tail under braking. Both looked like a handful at speed. It tells you something when Porsche aces like Pat Long were incredibly busy behind the wheel. And who doesn't love a 914?

{{< youtube iYJN-UXdrho >}}

## Bonus: Porsche 919 and 911 RSR

The 919 is quick. The speed trap showed 149MPH going into turn 5. While it doesn't howl like the 911 RSR, it still has a pleasingly aggressive exhaust note for a turbocharged car. The two cars were alone on track which meant that their sounds echoed around the valley. I was surprised at just how small and compact the 919 was as well.  It makes the 911 look obese in comparison. Factory drivers like Nick Tandy, Mark Webber, Brendon Hartley, Wolf Henzler, Jorg Bergmeister, Pat Long, and Earl Bamber were all in attendance and accessible to the fans.

{{< youtube ESJ7h2qlboI >}}