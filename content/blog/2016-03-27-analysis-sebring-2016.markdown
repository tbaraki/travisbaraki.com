---
author: travisba
comments: true
date: 2016-03-27 04:13:41+00:00
layout: post
link: http://www.travisbaraki.com/analysis/analysis-sebring-2016/
slug: analysis-sebring-2016
title: 'Analysis: Examining Timing Data From The 12 Hours of Sebring'
wordpress_id: 1366
categories:
- Analysis
---

The old airport runways of Sebring are an entirely different animal than the high-banks of Daytona. Rather than smooth asphalt, the cars were asked to navigate an uneven patchwork of several different surface materials, some portions dating back decades. The track puts intense stress on the car as well as the engineers and drivers tasked with finding a setup that works over the bumps. As such, it is a great opportunity to examine the relative performance of all the cars in a totally new environment and after several Balance of Performance (BoP) adjustments from IMSA.

<!-- more -->


#### Table of Contents

  1. Prototype
  2. GTLM
  3. GTD
  4. Race Overview & Class Comparisons
  5. Final Thoughts

## 1. Prototype

At [Daytona](http://www.travisbaraki.com/analysis/a-closer-look-at-the-24-hours-of-daytona/), the raw pace advantage of the two Ligier JS P2 entries was readily apparent. [IMSA reacted](http://www.imsa.com/sites/default/files/uploads/IMSA%20TECHNICAL%20BULLETIN%20IWSC%20%2316-16_01%20P%2C%20PC%2C%20GTLM%20%26%20GTD%20Balance%20of%20Performance.pdf) by reducing boost at the top-end of the Honda engine's rev range while simultaneously giving the Corvette DP a 10kg weight reduction and a larger air restrictor. The Ligier's boost ratio was [reduced further](http://www.imsa.com/sites/default/files/uploads/IMSA%20TECHNICAL%20BULLETIN%20IWSC%20%2316-19%20P%2C%20GTLM%20%26%20GTD%20Balance%20of%20Performance.pdf) after the Sebring test. The adjustments appear to have had the desired effect (Figure 1).

[caption id="attachment_1369" align="aligncenter" width="1151"][![Prototype - Fast Laps](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/P-Fast-Laps.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/P-Fast-Laps.png) **Figure 1:** The five fastest race laps for each car were averaged and compared to the average in class. Negative values are faster than class average; positive values are slower than class average. (Click to enlarge)[/caption]

While the two Ligier entries have again set the fastest laps of the race, the gap to the Corvette DP entries has closed significantly. At Daytona, the fastest of the DP runners was roughly a second behind the P2 cars. At Sebring, the DP cars were just four tenths back and in contention for the win.

[caption id="attachment_1372" align="aligncenter" width="1151"][![Prototype - Box Plots](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/P-Box-Plots.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/P-Box-Plots.png) **Figure 2:** Box plot showing the distribution of green flag lap times. Each box contains 50% of the car's laps with the median lap time denoted by the horizontal line. Median and fastest lap times are labelled. (Click to enlarge)[/caption]

Despite being a few tenths slower on ultimate pace, the DP cars compare quite favorably over the entire race distance (Figure 2). The median lap time of the DP group is a few tenths faster than that of the P2 cars. Perhaps the extra weight of the DP cars is an asset, helping tires get up to temperature on their outlaps as well as in the wet. Also of note, the Oreca's pace was quite comparable to that of the Ligiers and the Daytona Prototypes despite their Nissan engine being heavily restricted.

Unlike the DP teams, the ESM and Michael Shank P2 cars feature amateur drivers alongside professionals. This has often been highlighted as a potential reason for their lack of success, particularly in 2014 when the BoP between P2 and DP machinery was in its infancy. The Sebring weather made track conditions incredibly treacherous (for amateur and professional alike) and provided an opportunity for comparison (Figure 3).

[caption id="attachment_1374" align="aligncenter" width="1151"][![Prototype - Driver comp](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/P-Driver-comp.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/P-Driver-comp.png) **Figure 3:** Box plots showing drivers' green flag lap times. Each box contains 50% of the car's laps with the median lap time denoted by the horizontal line. (Click to enlarge)[/caption]

It is quite easy to pick out who ran in the wet, the dry, or a combination of the two. While at first glance this does not reflect well on Scott Sharp, there is a simple explanation: Sharp ran all of his laps just before and just after the red flag period while the track was at its wettest. Some turned laps in both the severe wet and the dry (Derani, Negri, Pruett) while others (Curran, Cameron, van Overbeek, Pla, Pew) predominantly drove on a dry or drying track. Pew's pace, while off the ultimate pace of the car, is consistent and compares fairly well to the much more experienced van Overbeek. The #60 might not always be in contention for the win on pace alone but is certainly close enough to capitalize on other teams' mistakes or misfortunes. ESM's Ed Brown is notably absent from this chart. He completed two laps, both under caution, before getting out of the car. While it would have been interesting to compare his pace to the others, the team can't be faulted for doing everything possible within the rules to secure the win.

## 2. GTLM

The GTLM field has already seen several BoP adjustments this season. After their 1-2 finish at Daytona, the Corvettes were penalized with a 10kg weight penalty, smaller air restrictor, and 5L smaller fuel tank. The BMWs had their boost ratio increased throughout the rev range and a 9L increase to tank capacity. The Fords have lost low and mid-range boost, gained top-end boost, gained 10kg, and lost 5L of fuel capacity. Porsche got some help in the form of a 10kg weight break and 7L of tank capacity. How have these changes impacted what was already a very competitive category? Not surprisingly, it is still very competitive (Figure 4).

[caption id="attachment_1379" align="aligncenter" width="1151"][![GTLM - Fast Laps](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/GTLM-Fast-Laps.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/GTLM-Fast-Laps.png) **Figure 4:** The five fastest race laps for each car were averaged and compared to the average in class. Negative values are faster than class average; positive values are slower than class average. (Click to enlarge)[/caption]

At Daytona, the entire field was within one second. By Sebring, and after the BoP adjustments, all cars set race laps within eight tenths of each other. If we compare the fastest lap set by each model of car (Figure 5), the gap closes further to only half a second. As diverse as the five cars are, this is very impressive. The BMWs are taking full advantage of their newly awarded boost. They impressed in qualifying and were quick throughout the race, a penalty for excessive boost likely costing the #25 the win. The Porsches, as they did at Petit Le Mans last season, hit their stride in the wet and changing conditions though it seems that their median dry pace is slightly off that of the others (Figure 5).

[caption id="attachment_1380" align="aligncenter" width="1151"][![GTLM Box Plots](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/GTLM-Box-Plots.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/GTLM-Box-Plots.png) **Figure 5:** Box plot showing the distribution of green flag lap times. Each box contains 50% of the car's laps with the median lap time denoted by the horizontal line. Median and fastest lap times are labelled. (Click to enlarge)[/caption]

## 3. GTD

The Lamborghini Huracan GT3 made a conspicuous debut. It was incredibly quick at Daytona but it was also in breach of the regulations which have since been clarified by IMSA: "All engine restrictor geometry must comply with the FIA homologated design and be registered and approved by IMSA prior to competition." In addition to the IMSA compliant air restrictor, the Lamborghini was also hit with a massive 40kg weight penalty. Meanwhile, the BMW was given a boost increase across the rev-range much like its GTLM-spec counterpart. Porsche received a 10kg weight increase while the Viper received a 15kg weight increase in combination with a larger air restrictor.

[caption id="attachment_1381" align="aligncenter" width="1151"][![GTD Fast Laps](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/GTD-Fast-Laps.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/GTD-Fast-Laps.png) **Figure 6: **The five fastest race laps for each car were averaged and compared to the average in class. Negative values are faster than class average; positive values are slower than class average. (Click to enlarge)[/caption]

Compared to Daytona, the performance spread of GTD field has narrowed significantly (Figure 6). The gap from fastest to slowest GTD car was approaching three seconds at Daytona — this spread has been halved for Sebring. As one would expect, the Lamborghini's 40kg weight penalty has had a massive impact on their pace. While dominant at Daytona, the fastest Huracan could only manage times seven tenths off those of the race winning Ferrari 488. The Ferrari was competing in its baseline BoP specification which seems to be somewhat generous. The median lap times (Figure 7) of all cars were within one second of each other. The Krohn Racing #45 was an outlier running the old Audi R8 LMS Ultra and was excluded from this figure.

[caption id="attachment_1384" align="aligncenter" width="1151"][![GTD Box Plots](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/GTD-Box-Plots-1.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/GTD-Box-Plots-1.png) **Figure 7:** Box plot showing the distribution of green flag lap times. Each box contains 50% of the car's laps with the median lap time denoted by the horizontal line. Median and fastest lap times are labelled. (Click to enlarge)[/caption]

## 4. Race Overview & Class Comparisons

After last season's soggy Petit Le Mans, which featured a Michelin-shod GTLM Porsche taking the overall win, Continental was expected to produce an improved rain tire for the 2016 season. The nasty weather that red-flagged the 12 Hours of Sebring gave us our first opportunity to determine if the new and improved Continental is up to the task.

[caption id="attachment_1385" align="aligncenter" width="1151"][![Prototype and GTLM Lap Progression](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/P-vs-GTLM.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/P-vs-GTLM.png) **Figure 8: **Green flag laps turned by Prototype (blue) and GTLM (red) entries throughout the 12 hour race session. Gaps along the X axis are the result of yellow and red flag laps being removed. (Click to enlarge)[/caption]

By plotting Prototype and GTLM lap time over the race distance (Figure 8), we can look at how times were influenced by the changing conditions. During periods of dry running, there was a distinct separation of Prototype and GTLM lap times. As the rain began, lap times increased and the gap between classes narrowed substantially. After the red flag period, the track began to dry and we saw the gap between classes begin to grow. As the rain returned, however, this separation again disappeared. The track eventually dried and the gap returned to its original state. While this is a marked improvement from last season, we can see that the GTLM cars are still very close to the Prototypes in wet conditions despite the improved Continental.

After the post-Daytona air restrictor drama, and the numerous restrictor and boost adjustments, I thought it would be interesting to examine the importance of top speed at both Daytona (Figure 9) and Sebring (Figure 10).

[caption id="attachment_1387" align="aligncenter" width="1151"][![Daytona Vmax](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/Vmax-Daytona.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/Vmax-Daytona.png) **Figure 9:** The top speed and lap time of each car's best three laps was plotted and a linear regression was drawn. A negative slope indicates a correlation between top speed and lap time. The R² value describes how closely the data points adhere to the regression line. A value of 1 is perfectly linear data. (Click to enlarge)[/caption]

As one would expect, Daytona puts a heavy premium on top speed. The effect is most clearly seen in the Prototype class (Figure 9). The data points adhere closely to the regression line and the slope is significantly negative. This is also observed in the GTD and GTLM classes, though the effect is not quite as pronounced.

Another insight to be taken from this plot is that the GTD, GTLM, and PC classes were all capable of very similar top speeds at Daytona. Furthermore, the GTLM and PC cars's lap times often overlapped. This corroborates what we all saw in the broadcast.

[caption id="attachment_1388" align="aligncenter" width="1151"][![Sebring Vmax ](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/Vmax-Sebring.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/Vmax-Sebring.png) **Figure 10:** The top speed and lap time of each car's best three laps was plotted and a linear regression was drawn. A negative slope indicates a correlation between top speed and lap time. The R² value describes how closely the data points adhere to the regression line. A value of 1 is perfectly linear data. (Click to enlarge)[/caption]

Top speed is clearly less of an asset at Sebring as compared to Daytona (Figure 10). The regression lines are close to horizontal. The strongest relationship appears to be in the GTLM class. Despite this, the difference in lap time between the fastest and slowest cars is minimal. This is not unexpected. As we have seen, the class is very tight and any performance advantage, no matter has small, is significant.

It is also worth noting that, unlike at Daytona, the PC cars are running faster lap times, and with a higher top speed, than the GTLM and GTD cars. The upgrades that IMSA has provided the PC cars seem to be helping to create some class separation even if some teams elected to run without traction control.

Sebring's turn 17 has been referred to as the toughest corner in racing. It is fast, long, bumpy, and the key to a quick lap. As such, I've used it as a litmus test to gauge how well the cars deal with the bumps and make lap time through Sebring's 17 turns (Figure 11).

[caption id="attachment_1391" align="aligncenter" width="1151"][![T17](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/T17.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/03/T17.png) **Figure 11: **Turn 17 sector times (sector 8) from each car's best three laps were plotted and a linear regression was drawn. A positive slope indicates a correlation between sector time and lap time. The R² value describes how closely the data points adhere to the regression line. A value of 1 is perfectly linear data. (Click to enlarge)[/caption]

Sector time through turn 17 appears to have a strong impact on overall lap time. The plot shows a positive slope for all four classes. Again, the effect is the least pronounced in the GTLM class. Interestingly, the PC cars seem very quick through turn 17 — they are quicker than the GTLM cars and as fast as the slower Prototypes (Mazda and Deltawing). This is further evidence that IMSA's PC upgrades have helped create some separation between classes.

## 5. Final Thoughts

Based on what we saw at Sebring, major BoP adjustments seem unlikely for the Prototype and GTLM cars. As it is, the BoP allowed for an exciting race that was decided by penalties, strategy, and driver skill in difficult conditions more than absolute pace. That said, the GTLM BMW may have received a boost increase that was slightly too generous. While the Ford hasn't quite been a match for its competitors, the gap should close as the program matures. In GTD, the Ferrari's baseline BoP seems very strong. It wouldn't be surprising if IMSA dials them back slightly going forward. Leave a comment and let me know if you agree or disagree. BoP has been extremely controversial over the past few seasons. It seems that IMSA has started to find the sweet spot for 2016.

Currently, my analysis has been focused on lap times. These are all impacted directly by air restrictor, weight, and boost BoP adjustments. I would like to find a way to quantify the impact fuel tank and fuel flow rate BoP adjustments have on the race. Please leave a comment with any ideas you may have. Any suggestions for how I can improve this analysis series would also be appreciated. Thanks for reading.