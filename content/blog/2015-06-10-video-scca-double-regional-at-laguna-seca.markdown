---
author: travisba
comments: true
date: 2015-06-10 02:28:03+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/video/video-scca-double-regional-at-laguna-seca/
slug: video-scca-double-regional-at-laguna-seca
title: 'Video: SCCA Double Regional at Laguna Seca'
wordpress_id: 414
categories:
- Video
---

I really enjoyed recording these cars. Since the Miata or Spec Racer Ford is so much quieter than a GTLM or DP car, you can really hear all the little details: gravel being tossed around the wheel wells, splitters scraping the track, bodywork rattling, tires chirping.

Mike Hedlund also brought his Pirelli World Challenge ride, a Ferrari 458, out to play – a nice sounding surprise.


