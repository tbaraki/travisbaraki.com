---
author: travisba
comments: true
date: 2015-07-12 19:01:22+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/video/chumpcar-at-mazda-raceway-laguna-seca/
slug: chumpcar-at-mazda-raceway-laguna-seca
title: 'Video: ChumpCar at Mazda Raceway Laguna Seca'
wordpress_id: 557
categories:
- Video
---

As promised, here are a couple videos of the ChumpCar action. I made some changes to my audio setup and am really happy with the results. The videos feature very long shots to capture the atmosphere and sound of the race. Also check out the [photos](http://www.travisbaraki.com/photo/gallery-chumpcar-laguna-chumpa-grand-prix/) from this event if you haven't already.






