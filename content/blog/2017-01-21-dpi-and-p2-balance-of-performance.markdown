---
author: travisba
comments: true
date: 2017-01-21 23:28:25+00:00
layout: post
link: http://www.travisbaraki.com/analysis/dpi-and-p2-balance-of-performance/
slug: dpi-and-p2-balance-of-performance
title: 'Analysis: DPi and P2 Balance of Performance'
wordpress_id: 1969
categories:
- Analysis
---

The big talking point for the 2017 IMSA WeatherTech United SportsCar Championship season is the introduction of the Daytona Prototype International (DPi) class. Much like 2014, where IMSA officials tried in vain to balance the performance of the now retired Daytona Prototypes and the fresh crop of LMP2 cars, the 2017 Prototype class will again be defined by its BoP.

The 2017 ACO-spec P2 cars are more powerful and more aerodynamically proficient than their predecessors. Using these platforms as a base, IMSA has tried to lure manufactures into DPi by offering a unique branding opportunity. Manufactures need not invest in an expensive R&D program to deliver a complete racecar capable of winning races overall. Instead, they are able to outfit an ACO-spec chassis with manufacture branded bodywork and engines and go racing. The skeptical fan would rightly presume that IMSA has an interest in keeping the DPi cars competitive relative to their spec P2 counterparts. What incentive would a manufacture have to stay involved if their entry is constantly beaten by an "off the shelf" model?

The Roar Before the 24 is the first opportunity for IMSA to gather data which will inform their BoP baseline heading into the 24 Hours of Daytona. Likewise, it is our first opportunity to gather some insight into IMSA's BoP posture and the direction of the class for the 2017 season.

## Roar Performance

{{< figure src="/img/dpip2/P-best-vs-optimal-1.png" link="/img/dpip2/P-best-vs-optimal-1.png" title="Figure 1:" caption="Comparison of each car's best lap to its optimal lap. Optimal lap is the sum of the car's best sector times." >}}

The Oreca 07 came off the truck fast, as did the #55 Mazda. Interestingly, the #70 was considerably slower than it's teammate, presumably running a different test program. The two Tequila Patron ESM Nissan DPi entries brought up the rear despite posting times only 1.2s off the leading Oreca. The Cadillac DPi posted very middle of the road times. It is worth noting that the #81 and #85 Oreca 07 entires have posted single sector times that suggest more speed in the bag. Perhaps the rest of the field is playing their cards close to the chest? Looking at sector times and top speeds, this absolutely seems to be the case.

{{< figure src="/img/dpip2/sector-map.png" link="/img/dpip2/sector-map.png" title="Figure 2:" caption="IMSA timing sectors for the 2017 24 Hours of Daytona. Sector 1 includes T1 and the infield section. The run up to and through the bus stop constitutes sector 2 while sector 3 is solely the full-throttle run to the start-finish line.">}}

{{< figure src="/img/dpip2/sectors-and-vmax.png" link="/img/dpip2/sectors-and-vmax.png" title="Figure 3:" caption="Each car's fastest sector time and highest top speed over the test." >}}

Again, the Oreca 07s are quick through sector 1 and especially quick in sector 2. Sector 3 is the most telling, however — it appears that very few cars are showing their full potential on the banking. Many were predicting speeds well over 200MPH. So far, the #55 Mazda is the only car to approach that benchmark (198.25MPH).

Despite the obvious manipulation by the teams, there are still conclusions that can be safely drawn. The Oreca 07 is fast whereas the Nissan DPi is not quite there yet. The Mazda continues to be competitive, a trend that started with their switch from diesel to gasoline power back in 2016. How the Cadillac DPi will fit into the order is yet to be seen. How much have they left on the table?

## BoP Changes

IMSA has made it clear that they intend to do the majority of their Daytona BoP through aerodynamic adjustments. For the most part, the changes made seem to correspond with the performance observed.

* The Nissan DPi, which was slowest overall and on the banking, was given a boost increase across the entire rev range as well as a decreased minimum rear wing angle. This is a significant boost to straight line performance. The increased boost was also complemented by an 5L increase in fuel tank capacity

* The Cadillac DPi was also given a decreased minimum wing angle, aiding top end performance. The height of its rear wing gurney has also been halved.

* In addition to a reduction in minimum wing angle, the VisitFlorida Riley/Multimatic P2 has also lost its rear wing flap gurney, aiding top speed. It was among the slowest on the banking.

Interestingly, the Mazda DPi, despite being the fastest car on the banking, has been given a reduced minimum wing angle on one of its two permitted aero configurations. The second configuration remains unchanged. They will also run the race with a 2L larger fuel tank as compared to The Roar. Boost levels remain unchanged.

On the whole, IMSA seems to have made pragmatic decisions when determining the baseline BoP for the 2017 Prototype class. Daytona is an anomaly on the calendar. It will be interesting to see how the BoP changes throughout the season.
