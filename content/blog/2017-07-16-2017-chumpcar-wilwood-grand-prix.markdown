---
author: travisba
comments: true
date: 2017-07-16 00:03:37+00:00
draft: false
layout: post
link: http://www.travisbaraki.com/photo/2017-chumpcar-wilwood-grand-prix/
slug: 2017-chumpcar-wilwood-grand-prix
title: 'Gallery: 2017 ChumpCar Wilwood Grand Prix'
wordpress_id: 2081
categories:
- Photo
---

I love this. I will definitely put a [ChumpCar](http://www.chumpcar.com) or [LeMons](http://24hoursoflemons.com) racer together one day. It's not a matter of _if_ but _when_.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/90679994@N08/albums/72157674951398798" title="2017 ChumpCar"><img src="https://farm2.staticflickr.com/1956/43768822180_1508fce704_b.jpg" width="1024" height="682" alt="2017 ChumpCar"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>