---
author: travisba
comments: true
date: 2016-05-21 17:05:57+00:00
layout: post
link: http://www.travisbaraki.com/analysis/timing-data-from-the-continental-tire-monterey-grand-prix/
slug: timing-data-from-the-continental-tire-monterey-grand-prix
title: 'Analysis: Timing Data From The Continental Tire Monterey Grand Prix'
wordpress_id: 1629
categories:
- Analysis
---

Mazda Raceway Laguna Seca rewards smooth and committed driving. Without the top speeds of Daytona or the stop-and-go cornering of Long Beach, cars and drivers need to keep their momentum up and maintain a high minimum speed through the corners. On paper, this should play right into the hands of the LMP2-based machinery.

#### Table Of Contents

  1. Prototype
  2. GTLM
  3. GTD
  4. Final Thoughts

## 1. Prototype

The resurgence of the Mazdas at Long Beach, a circuit that hardly suited the light and nimble prototypes, surely buoyed their confidence heading to their home track. The P2 machines have fared well against the Daytona Prototypes in the previous two post-merger events at Laguna Seca. 2016 was no exception.

{{< figure src="/img/imsa2016/P-Best-Laps.png" link="/img/imsa2016/P-Best-Laps.png" title="Figure 1:" caption="Each car’s five best laps were averaged and compared to the average in class. Negative values are faster than class average; positive values at slower than class average. (click to enlarge)" >}}

The P2 cars, particularly the Mazdas, were the class of the field. The Lola-Mazda package was capable of times well over half a second faster than the eventual race winning Ligier JS P2 of Michael Shank Racing (Figure 1). While the DeltaWing was predictably quick in a straight line (Figure 2), it was slower than its competitors through Laguna Seca's fast flowing downhill section (Figure 3).

{{< figure src="/img/imsa2016/P-Vmax.png" link="" title="Figure 2:" caption="Each car's five best trap speeds were averaged. Trap speeds were measured  entering turn one on the front straight. (click to enlarge)" >}} 

{{< figure src="/img/imsa2016/P-Downhill.png" link="/img/imsa2016/P-Downhill.png" title="Figure 3:" caption="For each car's five fastest laps, the average speed through sectors six and seven was calculated. This encompasses the entrance to the Corkscrew through to the entry of turn eleven. (click to enlarge)">}}

Unlike the DeltaWing, which traded cornering performance for top speed, the P2 cars have managed to find the best of both worlds. They were faster than the DP cars both down the straight and through the corners. Of course, laptime does not guarantee a good result. Just about everything that could go wrong did for the Mazdas. Pit stop errors, spins, and mechanical issues meant that their dominating performance did not translate into a victory.

It will be interesting to see if IMSA makes a BoP change going forward. While Laguna Seca very clearly plays to the strengths of the P2 cars, it does seem that Mazda has "found" some extra performance for their home race. The opposite may be true at Belle Isle. The circuit will likely favor the strengths of the Daytona Protoypes. A boost reduction for the Mazdas might not be out of the question.

**Edit:** _IMSA has indeed made a BoP adjustment to the Mazdas. They will be 15kg heavier for Belle Isle. It is worth noting that the Mazdas will still be 45kg lighter than the Ligier._

## 2. GTLM

The GTLM race was a thriller. From trackside, I had a difficult time keeping track of who pitted when and how far each contender could reasonably go. It was clear that they were in fuel-save mode for the latter half of the race; you could hear drivers roll off the throttle much earlier than normal and lap times plummeted. While the Ford GT secured its historic first victory through fuel economy, it is clearly a very competitive package all around.

{{< figure src="/img/imsa2016/GTLM-Best-Laps.png" link="/img/imsa2016/GTLM-Best-Laps.png" title="Figure 4:" caption="Each car's five best laps were averaged and compared to the average in class. Negative values are faster than class average; positive values at slower than class average. (click to enlarge)" >}}

The mid-engined Fords and Ferraris did very well at Laguna Seca (Figure 4). The BMWs, however, did not fare as well. They will surely be disappointed with their pace given the dominance of the Z4 at the 2015 race. Michelin put BMW in a difficult position by deciding to run the Le Mans spec tire. As the only team not being represented at Le Mans, BMW had not done any testing on that particular tire going into the race weekend. While this surely hurt them, the #25 managed times that compared well to the Porsches and the #3 Corvette.

Due to the struggling BMWs, and the incredible pace of the #68 Ferrari, the GTLM field was not as tight as we are used to seeing. The 1.2 second spread is fairly large especially considering the short lap.

{{< figure src="/img/imsa2016/GTLM-Vmax.png" link="/img/imsa2016/GTLM-Vmax.png" title="Figure 5:" caption="Each car's five best trap speeds were averaged. Trap speeds were measured entering turn one on the front straight. (click to enlarge)" >}}

The average top speeds of the GTLM field were all within 2 MPH of one another while the majority, including cars from each manufacturer, were all within 1 MPH of one another. With very similar top speeds, the difference in lap time would presumably come from performance through the corners.

{{< figure src="/img/imsa2016/GTLM-Downhill.png" link="/img/imsa2016/GTLM-Downhill.png" title="Figure 6:" caption="For each car's five fastest laps, the average speed through sectors six and seven was calculated. This encompasses the entrance to the Corkscrew through to the entry of turn eleven.(click to enlarge)" >}}

Indeed, the Ferraris did very well from the Corkscrew down to turn 11 (Figure 6). Notably, the Porsches, who performed very well on the technical sections of the Long Beach circuit, lagged behind everybody including the BMWs who were running on unfamiliar tires. Perhaps this is further evidence of Porsche's inability to fully take advantage of the new GTLM/GTE aerodynamic regulations at the rear of the car. The recently released photos of the new Porsche 911 GTE car conspicuously failed to document the car's rear end.

Looking into the fuel economy and stint length meant making a few assumptions. Firstly, we don't know how much fuel was left in the tank when they pitted. Second, we don't know how much fuel was left in the tank after the checkered flag. All cars made it back to the pits so they had at least 2.3 miles of slow running left.

Richard Westbrook managed to coax an estimated 4.895 MPG from the #67 Ford through the final 52 lap stint (table, below). It is also impressive that the #3 Corvette managed a practically identical 4.869 MPG on its final 50 lap stint. Interestingly, it seems that the difference in tank capacity corresponds almost exactly to the different in stint length given their similar fuel economy. While this "napkin math" is hardly evidence of a performance advantage, it raises questions about how tank size and refueling restrictors are treated in IMSA's BoP process. This is something that is not immediately evident in timing data. I would love to hear your thoughts on how an outside observer could best analyze this very important aspect of a car's race performance.

<table width="358" >
  <tbody >
    <tr >
      <th colspan="6" class="table_title" width="358" >Fuel Economy Comparison</td>
    </tr>
    <tr class="table_heading" >
      <th width="64" class="data_numerical" >Position</th>
      <th width="36" class="data_numerical" >Car</th>
      <th width="63" class="data_numerical" >Stops</th>
      <th width="57" class="data_numerical" >Longest Stint</th>
      <th width="55" class="data_numerical" >Tank Size (L)</th>
      <th width="83" class="data_numerical" >Est. Milage (MPG)</th>
    </tr>
    <tr class="data" >
      <td class="data_numerical" >1</td>
      <td class="data_numerical" >67</td>
      <td class="data_numerical" >1</td>
      <td class="data_numerical" >52</td>
      <td class="data_numerical" >90</td>
      <td class="data_numerical" >4.895</td>
    </tr>
    <tr class="data" >
      <td class="data_numerical" >4</td>
      <td class="data_numerical" >3</td>
      <td class="data_numerical" >2</td>
      <td class="data_numerical" >50</td>
      <td class="data_numerical" >87</td>
      <td class="data_numerical" >4.869</td>
    </tr>
    <tr class="data" >
      <td class="data_numerical" >6</td>
      <td class="data_numerical" >66</td>
      <td class="data_numerical" >2</td>
      <td class="data_numerical" >47</td>
      <td class="data_numerical" >90</td>
      <td class="data_numerical" >4.424</td>
    </tr>
    <tr class="data" >
      <td class="data_numerical" >10</td>
      <td class="data_numerical" >100</td>
      <td class="data_numerical" >1</td>
      <td class="data_numerical" >51</td>
      <td class="data_numerical" >103</td>
      <td class="data_numerical" >4.195</td>
    </tr>
  </tbody>
</table>

Westbrook's economy stint is even more impressive when compared to the final stint of his competitors. His pace, despite saving fuel, was very much comparable to other cars with fuel in hand (Figure 7).

{{< figure src="/img/imsa2016/GTLM-Eco-Line.png" link="/img/imsa2016/GTLM-Eco-Line.png" title="Figure 7:" caption="Green flag lap times were plotted to compare relative pace over each stint. (click to enlarge)">}}

When compared to the #68 Ferrari, who pitted two laps later than the #67, Westbrook's times look very similar. The #912 ran a much shorter final stint. While it did allow them to work their way up onto the podium, they did not have the time to catch the leader. This race appears to have been very hard on the Le Mans spec Michelin tires. At the end of the second stint, it is clear that times are starting to drop off long before fuel saving has become a concern. This trend continues dramatically through the long final stint. Lap times steadily slowed after the first 5-10 laps regardless of the fuel situation.

{{< figure src="/img/imsa2016/GTLM-Eco-Box.png" link="/img/imsa2016/GTLM-Eco-Box.png" title="Figure 8:" caption="Box plot showing the lap time distribution through the final, economy stint (striped) as compared to the second stint (solid) where fuel was not an issue. Each box the fastest half of the car's laps from that stint with the median lap time denoted by the horizontal line. (click to enlarge) ">}}

We can clearly see how much slower the long, final, economy stint was compared to the second stint earlier in the race (Figure 8). The #912 Porsche was never in fuel trouble. Despite this, it is clear that their pace was slower through the final stint. Despite setting their fastest laps of the race, the #912's median lap time through the final stint was comparable to the #67 and #68 who were both conserving fuel.

## 3. GTD

The GTD field was very tight at Mazda Raceway. The fastest laps of all 17 cars were covered by a 1.1 second spread (Figure 9).

{{< figure src="/img/imsa2016/GTD-Best-Laps.png" link="/img/imsa2016/GTD-Best-Laps.png" title="Figure 9:" caption="Each car's five best laps were averaged and compared to the average in class. Negative values are faster than class average; positive values at slower than class average. (click to enlarge) ">}}

It is great to see that one particular brand did not dominate the fast, or slow, end of the spectrum. The five fastest cars are from five different brands. It made for a wonderful race.

{{< figure src="/img/imsa2016/GTD-Vmax.png" link="/img/imsa2016/GTD-Vmax.png" title="Figure 10:" caption="Each car's five best trap speeds were averaged. Trap speeds were measured entering turn one on the front straight.Each car's five best laps were averaged and compared to the average in class. Negative values are faster than class average; positive values at slower than class average. (click to enlarge) ">}}

{{< figure src="/img/imsa2016/GTD-Downhill.png" link="/img/imsa2016/GTD-Downhill.png" title="Figure 11:" caption="For each car's five fastest laps, the average speed through sectors six and seven was calculated. This encompasses the entrance to the Corkscrew through to the entry of turn eleven. (click to enlarge) ">}}

Taken together, the trap speed (Figure 10) and average speed through the downhill portion of the track (Figure 11) show how well balanced the GTD field was. The Vipers were massively quick in a straight line but were among the slowest cars though the curves. Conversely, the Audis, Lamborghinis, and BMWs were slowest down the straight but performed very well through the fast corners. The Porsches were mid-pack in both categories. This is one of the most attractive aspects of GT racing. Certain cars will simply perform better at certain circuits. The ebb and flow of performance throughout the season keep the championship interesting and highlights the diversity of the field.

## 4. Final Thoughts

It seems that the BoP has mostly settled for the season. The huge performance swings of 2014 and 2015 have been replaced by smaller, data driven tweaks. It does seem that the Lamborghini's post-Daytona BoP adjustments were punitive rather than purely for performance equalization. They could probably stand to have their minimum weight dropped slightly. Aside from the raw pace disadvantage it brings, it also negatively impacts tire life. That said, any BoP changes IMSA makes from here should be made with an extremely gentle hand. We have seen great racing this season and it would be a shame to have that fact overshadowed by BoP politics.

This race also brought fuel strategy to the forefront. 4.9 MPG is hugely impressive in race conditions. For comparison, the fuel economy gauge on my car hovers around 10 MPG when merging onto the highway. My previous analyses have largely ignored fuel. The advantage fuel economy, tank size, or a refueling restrictor can impart is not readily apparent in timing data. If you have any ideas as to how this aspect of the racing can be examined in more detail, please let me know.
