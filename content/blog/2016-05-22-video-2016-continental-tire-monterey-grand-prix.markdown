---
author: travisba
comments: true
date: 2016-05-22 15:30:51+00:00
draft: false
layout: post
link: http://www.travisbaraki.com/video/video-2016-continental-tire-monterey-grand-prix/
slug: video-2016-continental-tire-monterey-grand-prix
title: 'Video: 2016 Continental Tire Monterey Grand Prix'
wordpress_id: 1690
categories:
- Video
---

I put together two videos from the footage I got at the race. First, a "Sights and Sounds" video featuring work in the pits as well as on-track action from both the WeatherTech and Continental Tire SportsCar Championships. Second, I wanted to do a sound comparison of the new-for-2016 GTLM cars. Two raucous V8s were replaced by turbos; I thought it would be interesting to compare.

## Sights and Sounds

{{< youtube vbGubOO7p1s >}}

## GTLM Comparison

{{< youtube SlVoF2AxyQc >}}
