---
author: travisba
comments: true
date: 2016-12-10 03:14:28+00:00
draft: false
layout: post
link: http://www.travisbaraki.com/photo/2016-nasa-25-hours-of-thunderhill/
slug: 2016-nasa-25-hours-of-thunderhill
title: 'Gallery: 2016 NASA 25 Hours of Thunderhill'
wordpress_id: 1903
categories:
- Photo
---

This is only my second year attending "The 25" but it is quickly becoming one of my favorite weekends of the year. This is true endurance racing; nobody is willing to throw in the towel. I watched shock changes, axle swaps, full engine pulls, and the Ryno Racing crew putting an entire right rear corner back on the car to salvage a second place overall, and class winning, finish. Congrats to the Flying Lizard crew for their overall win.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/90679994@N08/albums/72157699650267652" title="Thunderhill 2016"><img src="https://farm2.staticflickr.com/1937/44672719825_a8665db676_b.jpg" width="1024" height="682" alt="Thunderhill 2016"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>