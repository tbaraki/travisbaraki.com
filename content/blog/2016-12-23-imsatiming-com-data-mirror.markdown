---
author: travisba
comments: true
date: 2016-12-23 18:27:33+00:00
layout: post
link: http://www.travisbaraki.com/analysis/imsatiming-com-data-mirror/
slug: imsatiming-com-data-mirror
title: IMSATIMING.com Data Mirror
wordpress_id: 1960
categories:
- Analysis
post_format:
- Aside
---

After a brief scare this past week — imsatiming.com was temporarily inaccessible — I decided to archive and share the timing data. I know that I, and many others, appreciate the insight that can be found in the data.

[imsatiming.com mirror (Google Drive)](https://drive.google.com/drive/folders/0Bw70KEEKDF9TYmltN2ppZ1gzQ0U?usp=sharing)
