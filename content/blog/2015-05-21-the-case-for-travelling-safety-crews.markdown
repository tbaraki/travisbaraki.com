---
author: travisba
comments: true
date: 2015-05-21 02:00:49+00:00
layout: post
link: http://www.travisbaraki.com/opinion/the-case-for-travelling-safety-crews/
slug: the-case-for-travelling-safety-crews
title: 'Opinion: The Case for Travelling Safety Crews'
wordpress_id: 263
categories:
- Opinion
---

The decision by the Holmatro Safety Team to send James Hinchcliffe directly to IU Health Methodist Hospital, bypassing the infield medical center, likely saved his life. As has been demonstrated time and time again, IndyCar's dedicated safety crew is absolutely first rate.

And so they should be.
<!-- more -->
While no form of motorsport can ever be considered truly safe, IndyCar must surely be considered one of the most dangerous. The incredible speed, and proximity to the wall, dictates that any error, failure, or incident ends in a massive impact. The cars get faster each year yet the crazy meat-bag behind the wheel is as vulnerable as ever.

## Seconds Matter

In the inevitable event of a wreck, response time is critical. The sooner safety personnel can reach and assess the scene, the sooner decisions about care can be made. In contrast to IndyCar and the NHRA, IMSA no longer has a dedicated safety crew that travels with the series. Since the merger, they have relied on local emergency personnel. Multiple incidents through the TUDOR United SportsCar Championship's inaugural season have drawn attention to this change. At Sebring, Ben Keating's Viper burned for a full minute before crews arrived and another 20 seconds before the first extinguishing action was taken. Keating was able to safely extricate himself from the vehicle. Had he been tangled in his belts or, in the worst case, unconscious from an incident, the consequences of this delay may have been dire.

## Specialist Knowledge

Much like Keating, Andy Meyrick had his DeltaWing catch fire and eventually burn completely to the ground. The first responders were unaware of the specialized equipment found on racing cars. A DeltaWing mechanic had to approach the scene and turn off the master switch, cutting off the flow of fuel that had continued to feed the blaze. Similarly, those first on scene at the horrific accident between Matteo Malucelli and Memo Gidley were unprepared for the specific details of a severe racing incident. Dave Sims, team manager for Risi Competizione, said that a lack of familiarity with the car caused delays in removing the injured Malucelli. They did not know that the doors can be quickly removed or that the seat slides back for easier cockpit access.

This is in no way an indictment of the first responders involved. They are highly trained and deserve our respect for putting themselves in dangerous situations for our benefit. Racing is a very specialized, and high risk, sport and requires specialized attention when things inevitably go wrong. This is especially true in a mixed-make or multi-class series. Removing a driver from an open-wheel cockpit is inherently different than removing a driver from a closed-cockpit machine. Within a class, there are several different models with a multitude of significant variations to equipment location and function. Confusion costs seconds. In a time-critical situation such as a fire, or Hinchcliffe's recent gruesome vascular trauma, any delay could easily claim a driver's life.

## Familiar Faces

A dedicated travelling crew, like IndyCar's Holmatro Safety Team, has the detailed knowledge of the cars required to do their job in the most effective way possible. Since they travel with the series, they become friendly faces in the paddock. Teams and drivers take great comfort knowing that they will receive the best possible attention, from the most knowledgeable personnel. In addition to driver safety, the teams have a great deal to lose financially. As we saw with the Viper and DeltaWing fires, delays can result in the total loss of a several hundred thousand dollar racecar.

## IMSA

The ALMS and IMSA Safety Team have a spotless record. Since the merger, we have seen several incidents that raise serious questions about why the decision was made to disband them and rely on local personnel. The most recent, and graphic, example from Indianapolis reminds us that no matter how well designed the car and barriers are, the driver still faces a huge risk every time he goes out on track. IMSA teams and drivers would be better served with a travelling group of specialists like the team they once had and like the team IndyCar is thankful for today.
