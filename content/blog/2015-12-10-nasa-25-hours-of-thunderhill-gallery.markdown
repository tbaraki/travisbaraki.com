---
author: travisba
comments: true
date: 2015-12-10 02:51:36+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/photo/nasa-25-hours-of-thunderhill-gallery/
slug: nasa-25-hours-of-thunderhill-gallery
title: 'Gallery: NASA 25 Hours of Thunderhill'
wordpress_id: 1162
categories:
- Photo
---

My first trip to Thunderhill Raceway Park was wet, cold, and exciting. The large field, mix of pro and amateur drivers, and variety of cars (both fast and, _relatively,_ slow) meant that traffic was difficult from the drop of the green flag. For the most part, everybody was very well behaved. The Ginetta dropped back at first while the Norma, Eagle, and Radical entries stormed to the front with the Flying Lizard Audi never far behind. The Flying Lizards' reliability and experience was key as other teams succumbed to failure or penalty.

<!-- more -->

This race marked two "firsts" for me: shooting in the dark and shooting in the rain. While gear isn't everything, I definitely felt the need for a nice, fast, 2.8 zoom once the sun went down. The weather sealing of a higher end camera body would have been nice peace of mind as well, though my plastic bag did its job. The added drama of dark and wet conditions made for some great photos. While Thunderhill was slightly disappointing in terms of spectator access, the racing was fantastic. I definitely plan on making the drive again in 2016.

Note: I think the wet photos from Sunday morning are the most interesting. The gallery is in chronological order so make sure to click through them all.

[juicebox gallery_id="24"]
