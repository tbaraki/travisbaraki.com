---
author: travisba
comments: true
draft: true
date: 2015-04-05 03:27:30+00:00
layout: post
link: http://www.travisbaraki.com/video/video-microphone-test/
slug: video-microphone-test
title: 'Video: Microphone Test'
wordpress_id: 76
categories:
- Video
---

Having recently purchased a video-capable DSLR, finally joining the 21st century, I wanted to make sure that the sounds of the track are represented at least somewhat accurately. To that end I've picked up an Audio Technica stereo microphone. On a whim, I took a peek at the track and lucked out. There was a track day which brought a bunch of very cool, and very diverse sounding, cars to the area. I'm fairly pleased with the results. Far from perfect but I'm sure I'll improve with time.




