---
author: travisba
comments: true
date: 2016-04-24 19:48:37+00:00
layout: post
link: http://www.travisbaraki.com/analysis/long-beach-a-success-for-mazda-and-imsa-bop/
slug: long-beach-a-success-for-mazda-and-imsa-bop
title: 'Analysis: Long Beach a Success for Mazda and IMSA Balance of Performance'
wordpress_id: 1469
categories:
- Analysis
---

Unfortunately for Wayne Taylor Racing, their second consecutive Long Beach victory has largely been overshadowed by the Mazdas' return to form and the questionable driving of Porsche's Fred Makowiecki. Ricky and Jordan Taylor both drove fantastic stints and earned the first win for a Daytona Prototype in 2016. The Mazdas, finishing 4th and 5th, showed that they finally have the pace to race with the DP cars, beating the Michael Shank Racing Ligier JS P2 handily in the process. This fulfills the promise of their new gasoline-fueled engine, just in time for their home race at Mazda Raceway Laguna Seca.

<!-- more -->


#### Table of Contents

  1. Post-Sebring BoP
  2. Prototype
  3. GTLM
  4. Final Thoughts

## 1. Post-Sebring BoP

IMSA made a handful of minor, yet fruitful, [BoP adjustments](http://www.imsa.com/sites/default/files/uploads/IMSA%20TECHNICAL%20BULLETIN%20IWSC%20%2316-22%20P%2C%20GTLM%20%26%20GTD%20Balance%20of%20Performance.pdf) following Sebring. The Ligier JS P2 had it's boost reduced for the second time this season, most significantly at the top end of the rev range. In contract, the Mazdas were allowed slightly higher boost pressure at all engine speeds. The DeltaWing was given a 10kg weight break. Its dry weight is now less than half of its Corvette DP competitors (510kg vs 1029kg).

The GTLM field was largely left as-is, the exception being a boost reduction for BMW. Additionally, the Corvette, BMW, and Ferrari received tweaks to their refueling restrictors. BMW was allowed a slightly larger restrictor while Corvette and Ferrari had theirs reduced in size. Looking back to the [Sebring timing analysis](http://www.travisbaraki.com/analysis/analysis-sebring-2016/), all of these changes seem to be appropriate and pragmatic.

## 2. Prototype

For the first time this season, a Ligier JS P2 was not the fastest car on track (Figure 1).

[caption id="attachment_1478" align="aligncenter" width="1151"][![Figure 1:](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/Prototype-Laps.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/Prototype-Laps.png) **Figure 1:** Each car's five best race laps were averaged and compared to the average in class. Negative values are faster than class average; positive values are slower than class average. _(click to enlarge)_[/caption]

The #31 Action Express car was on a tear, setting a new race lap record en route to a podium finish, a full half second faster than what the #5 or #10 could manage. Notably, the Mazdas were capable of times very close to the race winning DP while the DeltaWing and Ligier, which were both strong at previous rounds this season, couldn't find pace around the streets of Long Beach.

[caption id="attachment_1485" align="alignnone" width="1097"][![Figure 2: ](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/Track-Map.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/Track-Map.png) **Figure 2:** Speed trap (T2) readings and average speed through sector 2 (red) were used to examine top-end speed. Average speed through sectors 4, 5, and 6 (blue) was used to compare cornering performance through the technical section of the circuit. _(click to enlarge)_[/caption]

To take a closer look at their relative performance, the track was divided into two sections (Figure 2). Time through sector 2 and speed trap readings were used to gauge top-end performance while average speed through sectors 4, 5, and 6 was used to compare performance through the tight, technical section of the track.

[caption id="attachment_1487" align="aligncenter" width="1151"][![Figure 3:](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/Prototype-Trap.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/Prototype-Trap.png) **Figure 3:** Speed trap readings (bottom, speckled) and average speed through sector 2 (top, solid). _(click to enlarge)_[/caption]

Despite another reduction in boost, the Ligier was again the fastest car in a straight line (Figure 3), though the difference was negligible. The Daytona Prototypes and the Mazdas were able to match it's speed through sector 2. Interestingly, the Mazdas and DeltaWing were quickest through the speed trap. Since the speed trap is relatively early in the straight, this suggests that the lightweight Mazdas and DeltaWing were able to get good traction and accelerate out of the hairpin well.

[caption id="attachment_1489" align="alignnone" width="1151"][![Figure 4:](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/Prototype-Technical.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/Prototype-Technical.png) **Figure 4:** Average speed through sectors 3, 4, and 5 (MPH). _(click to enlarge)_[/caption]

Clearly, the DeltaWing doesn't work well around the streets of Long Beach. The rest of the field was very close, however. The Corvette DPs, Ligier, and Mazdas were capable of running fairly comparable speeds through the technical portion of the circuit. As we saw at Sebring, the Ligier is very capable of dealing with the bumps. Perhaps the big V8 of the DP cars give it a slight edge getting out of the slower corners. Mazda's Lola chassis has the handling and they now seemingly have the speed to match (Figure 5).

[caption id="attachment_1493" align="aligncenter" width="1151"][![Prototype suspension](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/Mazda-Comparison.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/Mazda-Comparison.png) **Figure 5:** Comparison of the Mazda pace over the first three races of 2015 and 2016. Each car's five fastest laps were averaged and compared to those of a race winner. 100% is identical to the race winner; values >100% indicate a lack of pace compared to the winner. _(click to enlarge)_[/caption]

Compared to this point in the 2015 season, both Mazdas are considerably closer to the pace of the race winners. The switch away from the diesel powerplant has been a clear improvement for the team. Long Beach is significant as both cars were within 1% of the overall winner's pace, continuing a trend that started in Daytona. It will be interesting to see how they fare at Mazda Raceway Laguna Seca. It is a circuit that rewards smoothness. The Mazdas should do well especially now that they have additional power to get up the hill.

## 3. GTLM

As expected, the GTLM field was incredibly tight. All cars managed to set fast laps within a second of each other (Figure 6). If we ignore the struggling #62 car (which still managed to find its way to the podium), the field was separated by half a second.

[caption id="attachment_1497" align="aligncenter" width="1151"][![GTLM Laps](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/GTLM-Laps.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/GTLM-Laps.png) **Figure 6:** Each car's five best race laps were averaged and compared to the average in class. Negative values are faster than class average; positive values are slower than class average. _(click to enlarge)_[/caption]

It is interesting that the Porsches were the quickest cars, especially considering the comments made by Marco Ujhasi, Porsche's GT program manager:

<blockquote>"The 911 RSR performed excellently and it was clear to us that we would close the gap and win on our own merits..."</blockquote>

Conduct aside, they certainly had the pace. The Fords also showed well. They haven't got the results yet but clearly there is latent pace in their new GT.

[caption id="attachment_1499" align="aligncenter" width="1151"][![GTLM Trap Speeds](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/GTLM-Trap.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/GTLM-Trap.png) **Figure 7:** Speed trap readings (bottom, speckled) and average speed through sector 2 (top, solid). _(click to enlarge)_[/caption]

Indeed, the EcoBoost engine is working well. The Fords were capable of the highest speeds through the fast bend of sector 2 (Figure 7). The BMWs were still very competitive down the straight despite their post-Sebring boost reduction. While the Corvettes were slightly slower, the entire class performed comparably.

[caption id="attachment_1500" align="aligncenter" width="1151"][![GTLM Technical Section](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/GTLM-Technical.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/GTLM-Technical.png) **Figure 8:** Average speed through sectors 3, 4, and 5 (MPH). _(click to enlarge)_[/caption]

The Porsches made great time through the tight technical sections of the track. The traction afforded by their rear-engined layout seems to have given them an edge. The Fords were also quick through the corners, not just down the straight. It can't be long before they start getting some better results. Their sister cars were competitive at Silverstone to open the WEC season.

It's worth noting that the finishing positions of the cars don't match their pace. The obvious exception is the #911 but they needed a little bit of <del>subterfuge</del> help from their sister #912 to get through the #4 Corvette.

[caption id="attachment_1501" align="aligncenter" width="1151"][![GTLM Pit Lane Time](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/GTLM-Pits.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/04/GTLM-Pits.png) **Figure 9: **In-lap and out-lap times were summed for each car and plotted based on finish position. A trend line was drawn to show correlation between time spent on pit road and finishing position. _(click to enlarge)_[/caption]

Time spent in the pits appears to be a much better predictor of finishing position than raw pace (Figure 9). This is fairly intuitive. Long Beach is a short, one-stop race with very few passing opportunities. Track position is critical. Whereas the #62 managed to get on the podium by staying out of trouble and being efficient on pit lane the Fords and BMWs fell short despite having superior pace.

## 4. Final Thoughts

With the help of the new-to-2016 data loggers, IMSA seems to have found a competitive BoP formula that ensures quality competition. It may have taken two years, but the 2016 DP and LMP2 race well together — just in time for the major regulation overhaul for 2017. The GTLM field is as closely matched as ever. The huge performance swings of 2014 and 2015 seem to be replaced by minor, data-driven adjustments. It seems that every car genuinely has a shot at the podium.

What do you think? Is the BoP working or has IMSA still given a certain team an advantage?
