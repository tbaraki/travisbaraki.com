---
author: travisba
comments: true
date: 2017-09-28 01:59:24+00:00
draft: false
layout: post
link: http://www.travisbaraki.com/photo/2017-imsa-americas-tire-250/
slug: 2017-imsa-americas-tire-250
title: 2017 IMSA America's Tire 250
wordpress_id: 2163
categories:
- Photo
- Video
---

What a race. While the WEC implodes, IMSA is putting on a hell of a show. The DPi formula is attracting more manufacturers, GTLM is the best GT racing in the world, and the GTD field is quality from front to back. I'm glad I'm able to take advantage and attend the races.

<a data-flickr-embed="true"  href="https://www.flickr.com/gp/90679994@N08/17UBa6" title="IMSA 2017"><img src="https://farm2.staticflickr.com/1904/45536494552_91136dfcf4_b.jpg" width="1024" height="683" alt="IMSA 2017"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

{{< youtube id="cWsL-qXpuS0" >}}
