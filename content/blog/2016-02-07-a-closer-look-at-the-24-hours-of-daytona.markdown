---
author: travisba
comments: true
date: 2016-02-07 04:13:19+00:00
layout: post
link: http://www.travisbaraki.com/analysis/a-closer-look-at-the-24-hours-of-daytona/
slug: a-closer-look-at-the-24-hours-of-daytona
title: 'Analysis: A Closer Look at the 24 Hours of Daytona'
wordpress_id: 1301
categories:
- Analysis
---

What a fantastic start to the 2016 season. The massive 54 car field featured several highly revised or brand new cars. The racing was frantic with intense battles and wheel to wheel action from start to finish. As this was our first opportunity to see the 2016 cars being driven to their full potential, we can finally examine the true pace of each and how they stack up to the competition.

<!-- more -->


#### Table of Contents

  1. Prototype
  2. GTLM
  3. GTD
  4. Final Thoughts

## 1. Prototype

At [The Roar](http://www.travisbaraki.com/analysis/data-and-insights-from-the-roar-before-the-24/), the LMP2 based cars clearly had the advantage on ultimate pace. The same was true throughout the race (Figure 1).

[caption id="attachment_1303" align="aligncenter" width="1359"][![Figure 1: ](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/Prototype-best-laps.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/Prototype-best-laps.png) **Figure 1:** The best five race laps for each car were averaged and compared to the class average. Negative values are faster than class average; positive values are slower than class average. (Click to enlarge)[/caption]

The two Ligier teams (Michael Shank Racing and Tequila Patron ESM) were both capable of laps 0.8 seconds faster than the class average and nearly a second faster than the quickest Corvette DP. The two Ford Ecoboost Daytona Prototypes were also a few tenths quicker than the Corvettes. Most surprisingly, the DeltaWing had the speed to work through the field and into the lead. If not for Andy Meyrick's collision with a stopped PC car, and barring any reliability issues, the DeltaWing would surely have been in contention for a podium finish. The pace of the revamped Mazda prototypes is also noteworthy. Over a single lap, they were a match for any of the Corvette DP entries despite being a fairly dated chassis. The gasoline engine has proven to be a huge improvement over the previous seasons' diesel.

[caption id="attachment_1305" align="aligncenter" width="1343"][![Figure 2:](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/Prototype-distribution.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/Prototype-distribution.png) **Figure 2:** Box plot showing the distribution of green flag lap times. Each box contains 50% of the car's laps with the median lap time denoted by the horizontal line. The whiskers stretch down to the fastest lap and up to the slowest laps (95% percentile). (Click to enlarge)[/caption]

Of course, single lap pace does not tell the story of a 24 hour endurance race. It is also valuable to look at the distribution of lap times (Figure 2). Race pace for all teams was fairly similar. Most laps were turned in the mid 1:41 to low 1:42 range. The extra pace of the Ligier was apparent. They were the only cars to make it into the low 1:39s during the race.

While the Ligier may have had the advantage in outright pace, the Corvette DP entries were somewhat more consistent in their lap times. The Michael Shank entry in particular showed a huge variance in its lap times. We know that the heavier DP has an easier time putting heat into cold tires than the Ligier. This may have contributed to their consistency. Alternatively, one would expect the all-pro driver lineups of the DP teams to be more consistent than the amateur — FIA silver rated — drivers behind the wheel of the #60 Ligier.

From what we saw at Daytona, it would not be surprising to see IMSA make a BoP adjustment to the Ligier, perhaps a boost reduction. That said, Daytona does place an exceptional premium on acceleration and top speed. The performance balance may naturally look quite different at Sebring.

## 2. GTLM

The race debuts of the new Ford GT, Ferrari 488, and BMW M6 were highly anticipated. As expected, the performance of all cars was very closely matched (Figure 3).

[caption id="attachment_1312" align="aligncenter" width="1370"][![Figure 3:](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTLM-Best-Laps.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTLM-Best-Laps.png) **Figure 3: **The best five race laps for each car were averaged and compared to the class average. Negative values are faster than class average; positive values are slower than class average. (Click to enlarge)[/caption]

The top laps of the entire GTLM field were separated by around a second. The #3 Corvette's best laps were all turned in the final phase of the race while chasing its class-winning sister car (Figure 4). It can be assumed that without the draft from its teammate, the #3 would have posted times much closer to the the #4, narrowing the class spread even further.

[caption id="attachment_1313" align="aligncenter" width="1343"][![Figure 4:](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/3-vs-4.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/3-vs-4.png) **Figure 4:** Rolling 10-lap average of green flag pace of the #3 Corvette compared to the race winning #4 Corvette. Negative values indicate that the #3 was faster than the #4; positive values indicate that the #3 was slower than the #4. (Click to enlarge)[/caption]

[caption id="attachment_1314" align="aligncenter" width="1343"][![Figure 5:](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTLM-Distribution.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTLM-Distribution.png) **Figure 5:** Box plot showing the distribution of green flag lap times. Each box contains 50% of the car's laps with the median lap time denoted by the horizontal line. The whiskers stretch down to the fastest lap and up to the slowest laps (95% percentile). (Click to enlarge)[/caption]

The Ford, Ferrari, and BMW engineers can be satisfied that their brand new creations are a match for the tested and true Corvette and Porsche. Median lap times for all cars were fairly similar (Figure 5). More impressive, nearly every car was able to post times deep into the 1:44 range. The #67 Ford held the fastest lap in class for the majority of the race, until the #3 Corvette began mounting its charge. I wouldn't expect any major BoP adjustments for the GTLM runners, at least at this point in the season.

## 3. GTD

With IMSA's switch to FIA GT3 spec machinery, the GTD class is thriving. We are spoiled by both the size and diversity of the field. Of course, diversity means more work for the BoP folks. They will be busy.

[caption id="attachment_1318" align="aligncenter" width="1461"][![Figure 6:](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTD-Best-Laps2.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTD-Best-Laps2.png) **Figure 6: **The best five race laps for each car were averaged and compared to the class average. Negative values are faster than class average; positive values are slower than class average. (Click to enlarge)[/caption]

The timing charts were dominated by the Lamborghini Huracan (Figure 6). The quickest Huracan was 1.2 seconds faster than the next fastest make (Porsche). The top end speed of the Huracan was obvious from the TV feed. Through the speed trap, the Lamborghini was indeed the fastest (Figure 7) but not by the margin I would have expected.

[caption id="attachment_1319" align="aligncenter" width="1343"][![Figure 7:](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTD-Vmax.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTD-Vmax.png) **Figure 7:** The top five speed trap measurements for each car were averaged and compared. (Click to enlarge)[/caption]

While the Lamborghini was indeed very slippery, the Porsche and Ferrari were within reach. Despite the heavy BoP restrictions already placed upon the Viper's 8.3 liter V10, it was still capable of a very respectable top speed. The Audi, Aston Martin, and BMW clearly lack the top speed of the others. The winning #44 Audi proved that top speed isn't everything, even at Daytona, but they surely won't be happy with the disparity on the top end.

To take a closer look at the relative abilities of the GTD entries, we can separately examine average speeds through the slow infield and the fast high bank sections of the circuit (Figure 8).

[caption id="attachment_1322" align="aligncenter" width="1123"][![Figure 8: ](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/track-map-sectors.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/track-map-sectors.png) **Figure 8:** Track map showing the infield (blue) and oval (red) sections used for analysis. (Click to enlarge)[/caption]

Not only was the Huracan the fastest car through the speed trap, it also managed to post the highest average speed through the infield (Figure 9a). Clearly, it is also great on the brakes and putting the power down exiting the slow speed corners.

[caption id="attachment_1320" align="aligncenter" width="1343"][![Figure 9: ](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTD-Infield.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTD-Infield.png) **Figure 9a:** Average speed (MPH) through the infield (sectors 2 and 3). Data was taken from each car’s best five laps of the race. (Click to enlarge)[/caption]

As expected, the Huracan was fasted around the banking by a significant margin (Figure 9b). The BMW M6 was relatively slow in both GT3 and GTLM trim. This continues the trend from previous seasons where the nimble Z4 was capable of winning races (Laguna Seca 2015 comes to mind) but had a clear top speed deficit.

[caption id="attachment_1321" align="aligncenter" width="1343"][![Figure 9: ](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTD-Oval.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTD-Oval.png) **Figure 9b:** Average speed (MPH) through the oval excluding the bus stop (sectors 4-6 and 8-12). Data was taken from each car’s best five laps of the race. (Click to enlarge)[/caption]

Comparing the infield and oval data, we can see that some cars, such as the Ferrari and Viper, trade top speed for pace through the infield, the Porsche and Aston Martin are fairly well rounded, and the BMW is somewhat lacking at all points on the circuit. In contrast, the Huracan seems to be exceedingly quick everywhere.

Though Daytona certainly exaggerates the top speed advantage of the Lamborghini, IMSA will very likely adjust its BoP going forward. The BMW may get some help from the regulators as well. In any case, we can expect lots of great racing from the class going forward.

[caption id="attachment_1323" align="aligncenter" width="1343"][![Figure 10: ](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTD-Driver-Ratings.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/02/GTD-Driver-Ratings.png) **Figure 10:** Box plot showing the distribution of green flag lap times delivered by drivers of each FIA rating. Each box contains 50% of the category's laps with the median lap time denoted by the horizontal line. The whiskers stretch down to the fastest lap and up to the slowest laps (95% percentile). (Click to enlarge)[/caption]

The GTD entries also give us a chance to examine FIA driver rankings. It is no surprise that the professional drivers are more consistent than their amateur competitors (Figure 10). The bronze and silver drivers are clearly capable of putting in the lap time but the median lap time of the pro drivers was somewhat stronger though the race.

It will be interesting to see how this changes at different circuits. Perhaps the gap to the pros will increase at an ultra technical track such as Laguna Seca or Long Beach? I look forward to finding out.

## 4. Final Thoughts

The lap time data really doesn't tell the story of the race. So much time can be won or lost on pit strategy, navigating traffic, or due to a mechanical failure. Nonetheless, it is worthwhile to examine how cars and drivers have performed.

It seems that 2016 will be a classic season. It will be fascinating to see how the performance balance changes between racetracks and after the inevitable BoP adjustments. I fully expect that the championship will very closely fought in all four categories. I will be posting a similar analysis after the 12 Hours of Sebring.

If you have any comments or criticisms please leave a comment below. I'd love to hear from you.
