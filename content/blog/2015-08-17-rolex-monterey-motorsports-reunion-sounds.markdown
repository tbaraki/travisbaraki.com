---
author: travisba
comments: true
date: 2015-08-17 01:34:58+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/video/rolex-monterey-motorsports-reunion-sounds/
slug: rolex-monterey-motorsports-reunion-sounds
title: "Video: Rolex Monterey Motorsports Reunion"
wordpress_id: 749
categories:
  - Video
---

I've done things a little bit differently for this event. Instead of one massive compilation video, I've created two shorter videos, one for each of my favorite race groups. There is one for the FIA/IMSA GTP & GTO cars and one for the 1967-1984 Formula 1 cars.

<!-- more -->

#### 1967-1984 Formula 1

There was a lot of history through the 37-car grid. Cars that were once driven by the likes of Lauda, Hunt, Rosberg, Prost, Peroni, Alboreti, Jones, and Stewart were allowed to stretch their legs and really run. These were not at all parade laps. They raced and they raced hard. It was just amazing to see. And hear. Most cars were powered by the legendary Ford/Cosworth DFV but there were some 12-cylinder machines as well, namely a BRM and the Ferrari 312.

#### FIA/IMSA GTP & GTO

These cars were produced during an incredible competitive and innovative period of manufacturer-backed sportscar racing. There were some very interesting and ambitious designs on display. I am grateful to have had the opportunity to see the speed of the turbo Porsches and hear the shriek of the rotary-powered Mazda 787B, RX-792P, and RX-7. The ProCar BMW M1 was a surprise. It made such a outrageous scream. I got excited every time it came by.

#### Bonus

I thought I'd try something new. Here is an audio clip of the Formula 1 cars exiting The Corkscrew. I am very happy with the way the audio turned out. If you happen to have a toddler, and want them to be a racing driver when they grow up, put this on loop through the night. Your milage may vary.

[audio src="www.travisbaraki.com/audio/f1.m4a" loop="true" preload="auto"][/audio]

Audio download (.m4a) available right [here](https://drive.google.com/file/d/0Bw70KEEKDF9TSV81eHZHTTNQM0E/view?usp=sharing).

Also, be sure to check out the [photos](http://www.travisbaraki.com/photo/rolex-monterey-motorsports-reunion/) from this event if you haven't already.
