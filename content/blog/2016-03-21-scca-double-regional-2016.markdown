---
author: travisba
comments: true
date: 2016-03-21 17:05:45+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/photo/scca-double-regional-2016/
slug: scca-double-regional-2016
title: 'Video & Gallery: SCCA Double Regional Season Opener'
wordpress_id: 1334
categories:
- Photo
- Video
---

The SCCA's season opener was a low-key but welcome kick off to the 2016 season. I am a big fan of the Spec Miata and Spec Racer Ford. There were plenty of both out on track which made for some close racing and quite a few incidents. We've had quite a lot of rain over the past few weeks which meant that both the track and hillsides were very green.

<!-- more -->

[juicebox gallery_id="25"]



[youtube id="h4niUOg6zVQ" align="center" mode="lazyload-lightbox" autoplay="no" maxwidth="750"]
