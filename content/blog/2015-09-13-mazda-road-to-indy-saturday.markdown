---
author: travisba
comments: true
date: 2015-09-13 03:39:16+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/photo/mazda-road-to-indy-saturday/
slug: mazda-road-to-indy-saturday
title: 'Gallery: Mazda Road to Indy — Saturday'
wordpress_id: 824
categories:
- Photo
---

What an eventful day. Champions were crowned in two categories (USF2000 and Pro Mazda). Congratulations to Nico Jamin and Santiago Urrutia. Spencer Pigot inherited the Indy Lights lead when Max Chilton made an error entering The Corkscrew. The win puts Pigot in a strong position to claim the championship tomorrow. I can't wait to see how it unfolds.

<!-- more -->

[juicebox gallery_id="19"]
