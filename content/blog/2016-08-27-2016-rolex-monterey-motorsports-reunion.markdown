---
author: travisba
comments: true
date: 2016-08-27 16:20:08+00:00
draft: false
layout: post
link: http://www.travisbaraki.com/photo/2016-rolex-monterey-motorsports-reunion/
slug: 2016-rolex-monterey-motorsports-reunion
title: 'Gallery: 2016 ROLEX Monterey Motorsports Reunion'
wordpress_id: 1756
categories:
- Photo
---

This has absolutely become my favorite weekend of the year. The pedigree and preparation of the cars is hugely impressive. It's also refreshing to see these cars being driven hard rather than sitting in a museum. Kudos to the owners and drivers who allow their often priceless treasures to be driven in anger. It's not every day that you see a Ferrari 250 GTO with an armful of opposite lock exiting a corner. Fortunately, with names like Canepa, Jeanette, Said, and Auberlen behind the wheel, you know the cars are in good hands.

You may be able to tell from the photos that the 80s and 90s sports cars are my favorites. While they're "modern" in many respects, they still possess a brutality that we aren't allowed anymore. The gallery also includes audio from the F1 race on Sunday morning (don't worry; it won't autoplay). Enjoy.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/90679994@N08/albums/72157699651336272" title="ROLEX Reunion 2016"><img src="https://farm2.staticflickr.com/1946/45536680222_15fd3d2ab2_b.jpg" width="1024" height="682" alt="ROLEX Reunion 2016"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>