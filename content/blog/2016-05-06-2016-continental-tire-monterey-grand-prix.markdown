---
author: travisba
comments: true
date: 2016-05-06 03:36:05+00:00
draft: false
layout: post
link: http://www.travisbaraki.com/photo/2016-continental-tire-monterey-grand-prix/
slug: 2016-continental-tire-monterey-grand-prix
title: 'Gallery: 2016 Continental Tire Monterey Grand Prix'
wordpress_id: 1508
categories:
- Photo
---

This was my first opportunity to see the newly introduced GTLM cars from Ford, Ferrari, and BMW in person. The same can be said for the GT3-spec GTD cars as well. Both classes put on a great show. I'm already looking forward to next year and finding out what the prototype field will look like. I can only hope that it is as diverse and competitive as the GT fields.

As a bonus, the gallery also includes an audio clip to complete the experience. Click the speaker icon to enjoy the sounds of the PC and GTD cars as they exit The Corkscrew.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/90679994@N08/albums/72157701354856741" title="IMSA 2016"><img src="https://farm2.staticflickr.com/1956/44673498315_a0e7e91d4d_b.jpg" width="1024" height="682" alt="IMSA 2016"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
