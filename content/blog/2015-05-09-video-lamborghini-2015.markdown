---
author: travisba
comments: true
date: 2015-05-09 04:07:47+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/video/video-lamborghini-2015/
slug: video-lamborghini-2015
title: 'Video: Blancpain Lamborghini Super Trofeo Series'
wordpress_id: 180
categories:
- Video
---

The cars were already on track when I arrived at the circuit. The pack of screaming <del>Audi</del> Lamborghini V10 engines was the perfect opportunity to dial in the levels on my mics. It took a few passes, but I am pretty satisfied with the results. The video accurately reproduces the metallic exhaust note and violent peaks of the downshifts. The new Huracan was notably louder, and more abrasive, than the Gallardo with much sharper shifts. I certainly hope we get to see it line up against other GT3 machinery in the future.


