---
author: travisba
comments: true
date: 2016-09-04 18:01:00+00:00
draft: false
layout: post
link: http://www.travisbaraki.com/video/2016-rolex-monterey-motorsport-reunion/
slug: 2016-rolex-monterey-motorsport-reunion
title: 'Video: 2016 ROLEX Monterey Motorsport Reunion'
wordpress_id: 1824
categories:
- Video
---

I had to pick my shooting spots carefully. Unlike a 4-hour ALMS race, where I could wander, watch, and shoot from all points on the track, the compressed schedule of the reunion forced me to pick a spot for each race. Each race group is only on track for two 20-minute sessions — one on the morning and one in the afternoon. Since I was forced to choose, I chose the exit of turn 11 and The Corkscrew. Turn 11 to capture the sounds of the cars rocketing down the straight at full throttle and The Corkscrew because it is as iconic as it gets. I'm especially happy with the turn 11 footage. The cars sound great getting on the power and you can really see them moving around. They were not babied at all.

## 1963-1973 FIA Manufacturer's Championship

Cars featured: Porsche 917K, Porsche 908, and Ferrari 312PB

{{< youtube riRjwT77IK8 >}}

## 1981-1991 FIA and IMSA GT, GTO, and GTP

Cars featured: BVM V12 LMR, BMW M3, ProCar BMW M1, McLaren F1 GTR, Mazda RX-792, Mazda RX-7, and Porsche 962.

{{< youtube SyFe2qWv0Ig >}}

## 1867-1984 Formula One

Cars featured: Championship-winning Williams, Shadow, Ferrari, Lotus, and Stewart. Most cars were Ford DFV powered, the Ferrari flat-12 being a notable exception.

{{< youtube 9IV3K26Vpk8 >}}