---
author: travisba
comments: true
date: 2016-04-10 17:38:22+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/photo/video-gallery-historic-motor-sports-association-at-laguna-seca/
slug: video-gallery-historic-motor-sports-association-at-laguna-seca
title: 'Video & Gallery: Historic Motor Sports Association at Laguna Seca'
wordpress_id: 1409
categories:
- Photo
- Video
---

I went to the HMSA spring club event expecting a mix of vintage Porsche, Lotus, BMW, and Alfa Romeo entries. I absolutely did not expect to see the supremely dominant Audi R8 out on track. I had never seen the Audi in person before. It's a shame that world class and internationally relevant prototypes no longer have a place in US motorsports, aside from Lone Star Le Mans. Rose colored glasses?

<!-- more -->

[juicebox gallery_id="26"]



[youtube id="CnEwKkjjhIE" align="center" mode="lazyload-lightbox" autoplay="no" maxwidth="750" grow="yes"]


