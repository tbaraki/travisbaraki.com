---
author: travisba
comments: true
date: 2015-09-29 03:02:08+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/photo/porsche-rennsport-reunion-v-saturday/
slug: porsche-rennsport-reunion-v-saturday
title: 'Gallery: Porsche Rennsport Reunion V — Saturday'
wordpress_id: 989
categories:
- Photo
---

What better way for Porsche to celebrate their racing heritage than bringing together 60 years worth of extremely rare, race pedigreed cars and putting them out on track. These weren't parade laps. Cars were driven just as hard as they were decades ago. The event drew some star drivers too: Mark Webber, Brendon Hartley, Neil Jani, Cooper MacNeil, Wolf Henzler, Brian Sellers, Jeroen Bleekemolen, Hurley Haywood, Patrick Long, Marino Franchitti, Kevin Buckler, and other I'm sure I've forgotten were all in attendance.

<!-- more -->

Check out the [Sunday gallery](http://www.travisbaraki.com/photo/porsche-rennsport-reunion-v-sunday/) as well for more Porsche.



[juicebox gallery_id="22"]
