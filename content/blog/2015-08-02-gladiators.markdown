---
author: travisba
comments: true
date: 2015-08-02 14:06:31+00:00
layout: post
link: http://www.travisbaraki.com/opinion/gladiators/
slug: gladiators
title: 'Opinion: Gladiators'
wordpress_id: 579
categories:
- Opinion
---

I was massively excited to watch the FIM World Superbike races at Laguna Seca. It had been more than a decade since my last motorcycle race — inexcusable, I know. The bikes were already on track when we arrived on Saturday. The speed impressed immediately and really hammered home just how different, and dangerous, a motorcycle race is compared the car races I'm used to watching. Seeings the riders lean their bikes through turn 1 (what is essentially a straight in a car) at 150MPH gave me a sharp burst of adrenaline. I tried to imagine what the experience must be like for the riders, flying over the blind crest without the protection of a roll-cage or windscreen.

<!-- more -->

There was further evidence of the danger in the paddock, perhaps foreshadowing the tragic events of Sunday afternoon. We saw a sobering number of people confined to wheelchairs. Young people. I can't recall the same being true for any of the IMSA or IndyCar events I've been to. Amongst the team and manufacturer branded merchandise we found a box of shirts paying respect to the late Marco Simoncelli. In the grandstands, people seemed much more engaged with the riders. There were cheers every lap, not just for the leader or bold overtakes but for the whole field, an acknowledgment of the ludicrous amount of bravery required to compete at this level.

As the checkered flag waved on the FIM main event, I felt the usual satisfaction of having just been treated to a great race but there was something else. I felt relieved. Every wobble and slide was an opportunity for disaster and my heart skipped a beat every time. Davide Giugliano crashed heavily in turn 6, cartwheeling through the gravel. The medical staff were on the scene for several laps. He will miss the rest of the season with a [fractured T3 vertebra](http://www.worldsbk.com/en/news/2015/Davide+Giugliano+is+forced+to+interrupt+his+2015+WorldSBK+season).

We left the track before the start of the MotoAmerica Superbike/Superstock 1000 race. I didn't hear about the [crash](http://www.motoamerica.com/motoamerica-mourns-racing-tragedy) until the next morning. Daniel Rivas' bike lost power immediately after the start, initiating a chain reaction that eventually claimed his life as well as that of Bernat Martinez. While certainly not the first fatalities I've encountered as a racing fan, they have been on my mind daily. The immediacy of being there at the track, admiring the skill and courage of the riders and being cognizant of the risks, an hour before their lives were lost has definitely contributed to the impactfulness of the tragedy.

But there's something else: unlike other incidents, there isn't a proximal cause to deflect some of the emotion and discussion. When Senna was killed, there was talk about engineering negligence from the Williams team, tire pressures, and ride heights. Dale Earnhardt's crash put the focus on the HANS device and track barriers. Dan Wheldon's death resulted in IndyCar reevaluating speeds, tracks, and field sizes. Jules Bianchi's accident has kept us talking about safety procedures for months. Rivas and Martinez suddenly and publicly lost their lives due to a bad start. There's nothing to talk or think about other than the frailty of life, the grief of the families, and the heroism of the riders who, knowing the risks, put themselves out there every weekend for our entertainment and their love of the sport.

It's too easy to take the danger of racing for granted. Despite the procedural rigor and technological progress on display in modern motorsport, death is always in the shadows. More than any other athletes, racers are truly the gladiators of our age. I have nothing but respect for them and their craft.
