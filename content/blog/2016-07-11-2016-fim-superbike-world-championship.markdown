---
author: travisba
comments: true
date: 2016-07-11 19:33:04+00:00
draft: false
layout: post
link: http://www.travisbaraki.com/photo/2016-fim-superbike-world-championship/
slug: 2016-fim-superbike-world-championship
title: 'Gallery: 2016 FIM Superbike World Championship'
wordpress_id: 1701
categories:
- Photo
- Video
---

Insane speeds, impressive machines, and close battles in every single race — what a fantastic weekend. The talent required to wrestle a bike around Laguna Seca is mind boggling. It was a huge treat to see them up close and personal.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/90679994@N08/albums/72157697023657300" title="WorldSBK 2016"><img src="https://farm2.staticflickr.com/1961/45587189621_18ae22560c_b.jpg" width="1024" height="682" alt="WorldSBK 2016"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

The video really doesn't do justice to how fast these bikes are and how hard the riders work them through the turns. It was incredible to see it all happen from trackside.

{{< youtube iEgEK6S6nTE >}}
