---
author: travisba
comments: true
date: 2015-07-09 00:30:34+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/photo/gallery-chumpcar-laguna-chumpa-grand-prix/
slug: gallery-chumpcar-laguna-chumpa-grand-prix
title: 'Gallery: ChumpCar "Laguna Chumpa" Grand Prix'
wordpress_id: 476
categories:
- Photo
---

For those unfamiliar with ChumpCar, the [rules](http://chumpcar.com/rules.php) are simple: buy a car valued at under $500, kit it out with the required safety equipment, and go racing. While in practice the [regulations](http://chumpcar.com/downloads/2015ChumpCarBCCR.pdf) end up being slightly more complicated than that, the end result is a full field of evenly matched, race prepped beaters with loads of personality. The event was just as competitive as an SCCA weekend but with a refreshing, jocular atmosphere and a sense of humor.

<!-- more -->

ChumpCar ran three endurance races at Laguna Seca over July 4th weekend: a six-hour race on Friday and seven-hour races Saturday and Sunday. Teams and drivers came from all over the country with a strong contingent of Canadian racers included as well. If this sounds like something you want to be invloved in, and you have the requisite DIY attitude, head to [www.chumpcar.com](http://www.chumpcar.com) for more information and a full schedule. If you aren't in a position to build your own car, teams are often looking for drivers on the [forum](http://forum.chumpcar.com/) for specific events. This is something I would definitely like to do in the future.

I also took some [video](http://www.travisbaraki.com/video/chumpcar-at-mazda-raceway-laguna-seca/) of the race.

Special thanks to Bill and Sandi. I appreciate you having me.

[juicebox gallery_id="14"]
