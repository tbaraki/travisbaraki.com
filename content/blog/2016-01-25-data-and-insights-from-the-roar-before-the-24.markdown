---
author: travisba
comments: true
date: 2016-01-25 00:58:50+00:00
layout: post
link: http://www.travisbaraki.com/analysis/data-and-insights-from-the-roar-before-the-24/
slug: data-and-insights-from-the-roar-before-the-24
title: 'Analysis: Data and Insights From The Roar Before The 24'
wordpress_id: 1244
categories:
- Analysis
---

I've decided to try something new to kick off the 2016 season. After each IMSA <del>Tudor</del> WeatherTech SportsCar Championship event, I will be doing an analysis of the teams' relative performance and tracking how BoP adjustments have been applied. The goal is to learn a little bit about how the cars perform and how this performance is modified throughout the season.

<!-- more -->

#### Table of contents

  1. Prototype Data
  2. Prototype Balance of Performance
  3. GTLM Data
  4. GTLM Balance of Performance
  5. Class Comparison
  6. Conclusion

#### Disclaimer

As The Roar is a test, we should be cautious when drawing conclusions from the data. We have no way of knowing what sort of testing program the teams were adhering too; outright pace may not have been the goal. That said...

## Prototype Data

Despite IMSA's best efforts over the previous two seasons, there are still concerns surrounding the balance of performance between the LMP2-based cars and the Daytona Prototypes.

[caption id="attachment_1263" align="aligncenter" width="660"][![Figure 1: Session bests for each car were averaged and compared to the class average. Negative values are faster than the class average, positive values are slower. (Click to expand)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/Protoype-Relative-Averages-1-1280x761.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/Protoype-Relative-Averages-1.png) **Figure 1:** Session bests for each car were averaged and compared to the class average. Negative values are faster than the class average, positive values are slower. (Click to enlarge)[/caption]

At this very early point in the season, the P2 cars seem have the pace over the DP machines (Figure 1). The Tequila Patron ESM car and the SMP Racing BR01 have both adapted to the Continental tire very well. The DeltaWing also showed that it is capable of running competitively. As expected, the Mazda prototypes took a massive step forward by abandoning the SKYACTIV diesel for a race-bred, gasoline burning, turbo 4-cylinder. As we saw at last year's Daytona 24, outright pace does not necessarily translate into a win, or even the lead into T1, but the P2 machines will certainly be in the running for pole position once again.

[caption id="attachment_1265" align="aligncenter" width="660"][![](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/Prototype-Progression-1-1280x761.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/Prototype-Progression-1.png) **Figure 2:** Best times from each practice session are plotted and a linear trend is drawn. Daytona Prototypes and Mazdas found pace through the event while the Michael Shank JS P2 was fairly consistent. (Click to enlarge)[/caption]

The Ligier of Michael Shank Racing rolled off the truck and was fast right away (Figure 2). The Daytona Prototype and Mazda teams tended to improve as the event went on. It will be interesting to see if this trend will carry forward to practice, qualifying, and the race.

[caption id="attachment_1269" align="aligncenter" width="1123"][![Figure 3a: Track map showing the infield (blue) and oval (red) sections used for analysis. (Click to enlarge)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/track-map-sectors.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/track-map-sectors.png) **Figure 3a:** Track map showing the infield (blue) and oval (red) sections used for analysis. (Click to enlarge)[/caption]

To dive deeper into the data, I split the track into two sections in the hope that the relative strengths and weaknesses between the cars could be exposed. Average speeds through the infield (Figure 3a: blue) and oval (Figure 3a: red) were calculated and compared.

[caption id="attachment_1267" align="aligncenter" width="660"][![Figure 3a: Average speed through the infield (sectors 2 and 3). Data was taken from each car's weekend best lap. (Click to enlarge)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/prototype-infield-speed-1280x761.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/prototype-infield-speed.png) **Figure 3b:** Average speed (MPH) through the infield (sectors 2 and 3). Data was taken from each car's best lap of the weekend. (Click to enlarge)[/caption]

I chose to compare the fastest car of each "type" — a P2, a DP, a Mazda, and the DeltaWing. Surprisingly, the DeltaWing was the quickest through the infield section (Figure 3b). The Shank Ligier also performed well through the infield. Its highly developed aero and new, larger displacement engine surely helped.

[caption id="attachment_1271" align="aligncenter" width="660"][![Figure 3c: ](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/prototype-oval-speed-1280x762.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/prototype-oval-speed.png) Figure 3c: Average speed (MPH) through the oval (sectors 4-12). Data was taken from each car's best lap of the weekend. (Click to enlarge)[/caption]

Despite all cars having a similar Vmax (185-190MPH), the Ligier and Mazda were the quickest through the oval section (Figure 3c). This again confirms that the Mazda's new engine has been transformative. Since this section includes the bus stop, it may be that the DP just doesn't have the aero to get through the bus stop as quickly as the others. I would have expected the aero-efficient DeltaWing to be more competitive on the banking. While its Vmax was strong, perhaps the little Elan takes a bit longer to get up to speed. Of course, any draft effects are not considered and may have had a significant impact.


## Prototype Balance of Performance


IMSA's [finalized BoP](http://www.imsa.com/sites/default/files/uploads/IMSA%20TECHNICAL%20BULLETIN%20IWSC%20%2316-12%20P%2C%20PC%2C%20GTLM%20%26%20GTD%20Balance%20of%20Performance.pdf) for the 24 has been released and, based on observations from The Roar, seems quite reasonable. The Ligier and Mazda have seen their boost ratios decreased whereas the Corvette DP did not receive any BoP changes. It will be interesting to see what effect this has on the race. Historically, the DP has proven to be a better car on race day, despite the outright pace of the P2.


## GTLM Data


One thing is immediately apparent when looking at the GTLM data: the class is incredibly evenly matched.

[caption id="attachment_1278" align="aligncenter" width="660"][![Figure 4: ](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/gtlm-relative-averages-1280x762.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/gtlm-relative-averages.png) **Figure 4:** Session bests for each car were averaged and compared to the class average. Negative values are faster than the class average, positive values are slower. (Click to enlarge)[/caption]

Compared to the prototype data (Figure 1), the GTLM field is incredible tight (Figure 4) — both figures use the same y-axis scale (±1.5%). It is somewhat surprising to see both Porsches at the tail end of the field. However, since the gaps are so narrow, and this is a pre-season test, it doesn't seem prudent to pass judgement at this point.

[caption id="attachment_1279" align="aligncenter" width="660"][![Figure 5: ](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/gtlm-weekend-best-1280x762.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/gtlm-weekend-best.png) **Figure 5:** Best lap of the weekend for each of the GTLM entries. (Click to enlarge)[/caption]

All GTLM entries posted best times in the low 1:45s (Figure 5), 1.5-2 seconds slower than the 2015 pole time set by the #4 Corvette. Furthermore, at the 2015 Roar, GTLM teams were deep into the mid 1:44s. What does this mean?

[caption id="attachment_1280" align="aligncenter" width="660"][![Figure 6:](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/GTLM-Progression-1280x762.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/GTLM-Progression.png) **Figure 6:** Session best times for the three new turbo GTLM entries are plotted and a linear trend is drawn. (Click to enlarge)[/caption]

It certainly feels like the GTLM teams were keeping a close eye on one another. Nobody wanted to expose themselves and show their ultimate pace. One would expect that the three brand new, turbocharged cars would improve as the weekend progressed. This was not the case (Figure 6). This suggests that 1) they have a fantastic test program at the factory and 2) teams were <del>sandbagging</del> working on other aspects of their race. I wouldn't be surprised to see the GTLM pole time in the mid to high 1:42s.

[caption id="attachment_1285" align="aligncenter" width="660"][![Figure 7: ](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/GTLM-infield-speed-1280x762.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/GTLM-infield-speed.png) **Figure 7:** Average speed (MPH) through the infield (sectors 2 and 3). Data was taken from each car's best lap of Practice 6. (Click to enlarge)[/caption]

[caption id="attachment_1287" align="aligncenter" width="660"][![Figure 8: Average speed (MPH) through the oval (sectors 4-12). Data was taken from each car's best lap of the weekend. (Click to enlarge)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/GTLM-Oval-Speed-1280x762.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/GTLM-Oval-Speed.png) **Figure 8:** Average speed (MPH) through the oval (sectors 4-12). Data was taken from each car's best lap of the weekend. (Click to enlarge)[/caption]

Though the gaps are small, it is still interesting to see who did best through the infield (Figure 7) and who did best on the banking (Figure 8). Most teams set their best time in Practice 6. Those that didn't still managed to set times within a few hundredths of their best. For comparison's sake, the GTLM infield/oval comparison is using the fastest lap set by each model in Practice 6.

Interestingly, the fledgling Ford GT was the slowest of the bunch through the infield yet the fastest on the oval. The Porsche was relatively slow in both sections while the Corvette and BMW seem very competitive all around. Of course, the differences are minimal, and we have yet to see their true pace, so it is likely that things will change come qualifying.


## GTLM Balance of Performance


This is the first year that IMSA has had the benefit of detailed data logging on the GTLM machines. As such, and despite the cars running at what is presumed to be a conservative pace, IMSA has already made some [BoP adjustments](http://www.imsa.com/sites/default/files/uploads/IMSA%20TECHNICAL%20BULLETIN%20IWSC%20%2316-12%20P%2C%20PC%2C%20GTLM%20%26%20GTD%20Balance%20of%20Performance.pdf). All three turbocharged cars (Ford GT, BMW M6, and Ferrari 488) have received reductions to their boost ratios. Likewise, the Corvettes have also been giving a smaller air restrictor.

Its hard to make any sort of judgement on this first round of BoP adjustments. The cars all ran so closely and none ran to their full potential. Additionally, turbocharged and naturally aspirated cars are being balanced using different tools — boost ratios and air restrictors, respectively. This makes it harder for us on the outside to gauge the effect of the adjustments. It makes me wonder what IMSA saw in their data and what the pecking order will look like in the race.


## Class Comparison


[caption id="attachment_1289" align="aligncenter" width="660"][![Figure 9: ](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/class-comparison-1280x762.png)](http://www.travisbaraki.com/blog/wp-content/uploads/2016/01/class-comparison.png) **Figure 9:** Best times of each car were pooled by class and averaged as a means to compare relative pace between the four classes. (Click to enlarge)

Depending on how much faster the PC entries can go, the GTLM field may be right on their gearbox. The PC pole time in 2015 was 1:42.3. If the GTLM cars improve on their 2015, which I expect they will, then then gap between PC and GTLM will be uncomfortably small. Here's hoping that the 2016 PC improvements, especially those slated to arrive for Sebring, provide the desired results and speed the class up.


## Conclusion


While it is too early to draw any major conclusions about the teams' relative pace, we won't have long to wait. Once qualifying begins, we will have a clear picture as to where to teams stand relative to one another on outright pace. Of course, that can all change over the course of 24 racing hours.

I will be doing a similar analysis post Daytona including including the GTD cars. Please leave a comment with any feedback you may have. Did I make any grievous mistakes? Can I improve my stats? Anything I didn't measure that you would like to see? Let me know and I'll do my best.
