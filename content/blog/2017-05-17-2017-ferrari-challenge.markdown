---
author: travisba
comments: true
date: 2017-05-17 20:15:22+00:00
draft: false
layout: post
link: http://www.travisbaraki.com/photo/2017-ferrari-challenge/
slug: 2017-ferrari-challenge
title: 'Gallery & Video: 2017 Ferrari Challenge'
wordpress_id: 2018
categories:
- Photo
- Video
---

This race was the North American debut for the 488 Challenge. It can't match the scream of the outgoing 458 (of which there were many) but it looks amazing.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/90679994@N08/albums/72157701352903481" title="2017 Ferrari Challenge"><img src="https://farm2.staticflickr.com/1974/30645369407_b0905345d8_b.jpg" width="1024" height="682" alt="2017 Ferrari Challenge"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

{{< youtube EkeVu4CxDdw >}}