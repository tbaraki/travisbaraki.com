---
author: travisba
comments: true
date: 2015-06-11 00:56:02+00:00
layout: post
link: http://www.travisbaraki.com/opinion/opinion-fords-gt-program-driver-lineup/
slug: opinion-fords-gt-program-driver-lineup
title: 'Opinion: Who will drive for Ford''s soon-to-be-announced GT program?'
wordpress_id: 419
categories:
- Opinion
---

Two days from now in Le Mans, Ford will formally announce what many have long suspected. Taking aim at Chevrolet, Porsche, Ferrari, and Aston Martin, the Blue Oval will return to international sportscar racing with the new, Ecoboost-powered, Ford GT.

<!-- more -->The car is expected to compete in both the TUDOR United SportsCar Championship (GTLM) as well as the WEC (GTE). With the 2016 24 Hours of Le Mans as the target, Chip Ganassi Racing (CGR), in partnership with Multimatic and Roush Yates, has been chosen to execute Ford's vision. CGR will likely run the North American program. Multimatic, a Canadian company, has a UK base of operations and is the logical choice to conduct European operations. While details have yet to be released, CGR and Multimatic have a deep talent pool from which to pull drivers.

#### Sure Things

Almost certainly, the Ford program will rely on the vast experience of Scott Pruett. A five-time winner of the 24 Hours of Daytona and GTS class winner at Le Mans, Pruett is the obvious choice to lead the charge to Le Mans. Likewise, the international and endurance racing experience of Joey Hand will be invaluable. Having won both Daytona and Sebring, and secured a GTE Pro podium at Le Mans, CGR will surely want to have him in a Ford. How this will be handled by CGR and BMW is far less certain.

#### Likely Candidates

Despite Sage Karam's 2015 IndyCar campaign, CGR should have enough faith in his speed and racecraft to include him on the Le Mans roster. He laid down some incredibly quick and clean stints in the 24 Hours of Daytona. Karam appears to be a rare talent that will grow immensely with experience.

Continuing to use Daytona as an example, CGR has shown that they expect their IndyCar and NASCAR drivers to deliver results in sportscars. Scott Dixon, Tony Kanaan, Kyle Larson, and Jamie McMurray took the overall victory. Charlie Kimball also put in a fantastic performance in the sister car alongside Pruett, Hand, and Karam.

Multimatic's CTSCC drivers Billy Johnson, Austin Cindric, and Scott Maxwell should also be considered. All have performed exceptionally well in the Ford Mustang BOSS 302R. Cindric, still a teenager, shows immense promise. He has already successfully raced a diverse list of cars and, like Karam, will only improve with experience. Maxwell, in addition to his CTSCC drive, has reportedly been testing the new Ford GT. That experience, and his considerable resume, make him a great choice to occupy a race seat.

#### Wild Speculation

Kuno Wittmer and Jonathan Bomarito, former GTLM Viper drivers, would surely jump at the chance to get back into a competitive, factory-backed, international ride. Wittmer is currently driving in the Continental Tire SportsCar Challenge (CTSCC) while Bomarito has moved to the TUDOR United SportsCar Championship's Speedsource Mazda SkyActiv prototype. Both have proven themselves to be effective behind the wheel of the GTLM Viper, not just in race conditions but throughout the development process.

Perhaps most interesting are the rumors that Ford has managed to sign drivers away from other teams. This may include Kévin Estre who has done well in Porsches and McLarens. Fueling the rivalry between Ford and Chevy, it is not inconceivable that Richard Westbrook could defect to the Blue Oval. Having been denied (four times) a Le Mans class win in a Corvette, Westbrook might fancy giving it a go as part of a burgeoning program. This is especially true as the future of prototype racing in North America is quite hazy. Another possible addition is Pierre Kaffer. With Le Mans experience in GT2/GTE, LMP2, and now LMP1, he would be a very strong addition to the roster. Considering that the Ford will surely be more competitive in class than his current ByKOLLES P1 ride, he may have good reason to make the switch.

_Revised June 11, 2015_
