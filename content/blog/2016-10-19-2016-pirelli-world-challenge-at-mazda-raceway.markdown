---
author: travisba
comments: true
date: 2016-10-19 02:49:50+00:00
draft: false
layout: post
link: http://www.travisbaraki.com/photo/2016-pirelli-world-challenge-at-mazda-raceway/
slug: 2016-pirelli-world-challenge-at-mazda-raceway
title: 'Gallery: 2016 Pirelli World Challenge At Mazda Raceway'
wordpress_id: 1841
categories:
- Photo
---

Another year, another dramatic finish to the Pirelli World Challenge season. I watched the final laps from The Corkscrew. I thought the championship was won. I feel for Pat Long but Alvaro Parente was fast all season and I always love seeing the K-Pax guys do well.

The RealTime Racing Acura TLX will now be relegated to museum duty. Fortunately, that means the NSX GT3 will be on track for the 2017 season, in both IMSA and PWC competition, which is a great thing.

Next stop: Thunderhill.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/90679994@N08/albums/72157674952921178" title="PWC 2016"><img src="https://farm2.staticflickr.com/1907/43769501550_5264906080_b.jpg" width="1024" height="682" alt="PWC 2016"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>