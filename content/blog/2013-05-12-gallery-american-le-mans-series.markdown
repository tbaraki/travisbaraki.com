---
author: travisba
comments: true
date: 2013-05-12 02:54:55+00:00
draft: false
layout: post
link: http://www.travisbaraki.com/photo/gallery-american-le-mans-series/
slug: gallery-american-le-mans-series
title: 'Gallery: American Le Mans Series'
wordpress_id: 24
categories:
- Photo
---

My first (and final) American Le Mans Series race was a blast. Great on-track action was complimented by amazing paddock access. The diversity of the cars was a treat.

Taking photos was a challenge. Laguna Seca offers great vantage points for a spectator but I quickly ran out of talent. My success rate was completely abysmal while trying to get good pans. Fortunately, even a blind mouse finds cheese sometimes.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/90679994@N08/albums/72157633489849876" title="ALMS 2013"><img src="https://farm8.staticflickr.com/7295/8736948624_6fd28feb6a_b.jpg" width="1024" height="681" alt="ALMS 2013"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
