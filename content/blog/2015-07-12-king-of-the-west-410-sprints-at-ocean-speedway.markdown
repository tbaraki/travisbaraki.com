---
author: travisba
comments: true
draft: false
date: 2015-07-12 19:44:35+00:00
layout: post
link: http://www.travisbaraki.com/video/king-of-the-west-410-sprints-at-ocean-speedway/
slug: king-of-the-west-410-sprints-at-ocean-speedway
title: 'Video: King of the West 410 Sprints at Ocean Speedway'
wordpress_id: 561
categories:
- Video
---

After this year's IndyCar race at <del>the deathbowl</del> Fontana, fans and pundits were divided. Was this race too dangerous? Should IndyCar make changes to avoid similar "pack racing" in the future? As Robin Miller pointed out in his article ["Danger with a capital D"](http://www.racer.com/more/viewpoints/item/118626-miller-danger-with-a-capital-d) for RACER.com, the division followed obvious lines in the paddock:

<blockquote>The divided camps were easy to identify. Everybody with USAC roots and sprint car ties loved the action. "Winchester was always a pack race at 130mph on a half-mile," said Dane Carter, former driver and son of USAC legend Pancho. "That's what racing is all about," said Jeremy Milless, engineer for Newgarden and a former midget racer.</blockquote>

The power, speed, and required talent is just insane. It's easy to see how this experience would shape a person's perception and tolerance for danger. Several incidents resulted in cars flipping. Thankfully all the drivers were uninjured. What a show.

{{< youtube ulUMXBha3oc>}}