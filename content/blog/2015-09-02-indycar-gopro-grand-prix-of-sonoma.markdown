---
author: travisba
comments: true
date: 2015-09-02 01:53:06+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/photo/indycar-gopro-grand-prix-of-sonoma/
slug: indycar-gopro-grand-prix-of-sonoma
title: 'Video & Gallery: IndyCar GoPro Grand Prix of Sonoma'
wordpress_id: 780
categories:
- Photo
- Video
---

What a race. What a season. Yet again, the championship is decided in the final race. A lot of things went right for IndyCar this season. There were some truly great races and a dramatic championship fight that helped pull TV ratings out of the gutter. After some initial despair, Honda teams were able to get back on relatively even terms with their Chevy counterparts. Even still, a lot of things went wrong this season as well. Inconsistent officiating, excessive caution periods, and the terrible accidents that claimed the life of Justin Wilson and very nearly James Hinchcliffe have cast a shadow on the 2015 season.

<!-- more -->

The pre-race tribute to Wilson was appropriately stoic. A video tribute was played, his number 25 was drawn by a skywriter, the British national anthem was played, a moment of silence was observed, and then we went racing. The cars all displayed Justin's number 25 on their LED display for the pace laps. It was nice to see lots of [memorial shirts](http://www.manifestgroup.com/justin-wilson-tribute-tee/) and [decals](http://shop.ims.com/indycar/drivers/justin-wilson/) around the track. It was great to see so many people [contributing](http://justinwilson.co.uk/donate) to make sure his children will be well taken care of.

The on-track action was anything but stoic. You couldn't write a better script for a season finale (well, maybe Massa/Hamilton 2008). Championship teammates and title contenders coming together? Bourdais reverting to his youthful form and taking out yet another contender? Double points became moot, thankfully, but they certainly added to the intensity.

I hope that IndyCar can keep the momentum going into next season.



[juicebox gallery_id="17"]
