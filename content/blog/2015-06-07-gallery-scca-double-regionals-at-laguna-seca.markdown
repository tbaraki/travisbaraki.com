---
author: travisba
comments: true
date: 2015-06-07 05:27:38+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/photo/gallery-scca-double-regionals-at-laguna-seca/
slug: gallery-scca-double-regionals-at-laguna-seca
title: 'Gallery: SCCA Double Regional at Laguna Seca'
wordpress_id: 358
categories:
- Photo
---

I've been looking forward to this for some time now. The various SCCA classes sharing the track means a very diverse field and lots of action. The train of Spec Racer Fords sliding through the Corkscrew and Rainey is always entertaining.

<!-- more -->

As the morning fog burned off the lighting was highly variable. I missed a lot of shots by not paying enough attention to the changing conditions. Still, I'm happy that I covered most of the classes and got a few that I really enjoy.

[juicebox gallery_id="13"]
