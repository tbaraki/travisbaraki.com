---
author: travisba
comments: true
date: 2015-09-14 01:54:06+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/photo/pirelli-world-challenge-and-mazda-road-to-indy/
slug: pirelli-world-challenge-and-mazda-road-to-indy
title: 'Gallery: Pirelli World Challenge and Mazda Road to Indy — Sunday'
wordpress_id: 933
categories:
- Photo
---

Spencer Pigot drove a flawless race to secure his Indy Lights championship. Sean Rayhall was never far behind but didn't have the pace to challenge for the race win today. Congratulations to Spencer and Juncos Racing. The Pirelli World Challenge GT championship ended as expected: dramatic twists and turns, hard racing, and Berretta and O'Connell coming together. I think we all saw it coming. Also as expected, Christina Nielsen got another great finish. What a year she's having.

<!-- more -->

[juicebox gallery_id="21"]
