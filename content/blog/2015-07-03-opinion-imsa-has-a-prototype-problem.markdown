---
author: travisba
comments: true
date: 2015-07-03 16:39:39+00:00
layout: post
link: http://www.travisbaraki.com/opinion/opinion-imsa-has-a-prototype-problem/
slug: opinion-imsa-has-a-prototype-problem
title: "Opinion: IMSA Has a Prototype Problem"
wordpress_id: 445
categories:
  - Opinion
---

IMSA and the TUDOR United SportsCar Championship are, once again, having an identity crisis. Heading into the 2014 season, IMSA needed to find a way for ALMS LMP2 and Grand-Am Daytona Prototype entries to coexist. A season and a half later, things on track are looking pretty good. While there are still balance of performance issues to be finessed (top-speed, pace on restarts), both platforms have proven to be capable of topping timesheets and winning races.

The 2017 season will bring sweeping changes to both ACO and IMSA prototype regulations. IMSA has once again found themselves tasked with reaching a compromise between stakeholders with vastly differing objectives. For the FIA and ACO, the 2017 regulation changes will try to steer the category towards a more financially accessible future. In stark contrast to their European associates, IMSA may well be fighting to keep prototype racing viable in the North American market.

<!-- more -->

{{< figure src="/img/imsaproto/muscleHPD.jpg" caption="The HPD ARX-03c of Muscle Milk Pickett Racing competing in the ALMS P1 class (2013)" >}}

#### What Europe Wants

The crown jewel of WEC competition is the manufacturer-backed LMP1 category. With hybrid power units providing significantly more power than modern Formula 1 specifications will allow, swift and contentious aerodynamic development, and the full engineering resources of Porsche, Audi, Toyota, and Nissan on display, LMP1 appeals both to fans of prototype sportscar racing and manufactures looking to showcase and develop their technologies. It is against this backdrop that the FIA and ACO have considered the future of their LMP2 category.

For 2017, FIA/ACO LMP2 teams will choose a chassis built by one of four chassis constructors and wrapped in spec bodywork. While the ACO has yet to declare which constructors will be included (a decision is expected in the coming weeks), Onroak/Ligier and Oreca will surely be included and it is somewhat likely that Riley Technologies will also be given consideration. Furthermore, all entries will be powered by a spec V8 engine. The significant initial investment required by teams to buy in to the new formula is designed to be offset by the much more manageable running costs of a mostly spec series.

This cost-effective, mostly spec, pro-am strategy makes perfect sense in the European market and the WEC in particular. LMP1 will continue to be the proving ground and marketing arena of major manufacturers while LMP2 will be the category of choice for ambitious privateers and well funded gentlemen drivers. The loss of constructor diversity is certainly regrettable – names like Morgan, Dome, Gibson, Pescarolo, and HPD will be sorely missed – but the ACO's priority is ensuring full fields and tight competition. Whether or not the category needed a massive overhaul to accomplish these goals is up for debate.

{{< figure src="/img/imsaproto/shankP2.jpg" caption="Michael Shank Racing's Ligier JS P2 at Mazda Raceway Laguna Seca (2015)" >}}

#### The Compromise

ALMS fans are accustomed to seeing American teams compete at Le Mans and European teams try their hand at the great American endurance events. Most recently, Corvette Racing had an eventful 2015 Le Mans campaign, losing one car and taking the GTE win with the other. This season has also seen Extreme Speed Motorsports and Krohn Racing take their cars to Europe in search of greener pastures. To this end, the ACO and IMSA seemingly share a common interest: develop an internationally-relevant category that provides a great value to prospective customers. Both series have an interest in maintaining full grids. The lofty goal of a universal prototype category has brought the two sanctioning bodies together despite their vastly different needs.

IMSA is operating in a much different marketplace than the ACO. Whereas the ACO has LMP1 as its premiere category, the new LMP2 regulations will form the foundation of IMSA's top class. It is in this category that IMSA <del>hopes</del> needs to attract manufacturer interest and participation while ACO regulations expressly prohibit direct manufacturer involvement. To this end, IMSA and the ACO have agreed upon certain provisions that will give IMSA teams and manufacturers the freedom to innovate in an otherwise spec series. IMSA President, Scott Atherton, speaking to John Hindhaugh during the Sahlen's Six Hours of The Glen broadcast, outlined the new regulations as modified for IMSA competition:

- Teams will still choose from one of the four chassis constructors

- Teams will be able to run modified bodywork with the goal being brand identity rather than aerodynamic advantage (Marshall Pruett has written a [fantastic overview](http://www.racer.com/imsa/item/118705-imsa-first-look-at-2017-prototype-regulations)) and to accommodate various engines

- Teams are able to run their own engines in place of  the spec V8

However, there are complications if an IMSA team wishes to race at Le Mans or other ACO sanctioned events. The IMSA team would be allowed to run their non-spec engine, but must revert to the spec bodywork. This raises two important, but not insurmountable, issues. Will running the spec bodywork compromise engine cooling? As IMSA engines will be significantly more powerful, how will the ACO balance their performance against the spec unit? Non-spec cars running in a spec class creates a contentious situation that, if not handled correctly, will turn teams off of the category.

#### What America Needs

In short, what IMSA needs more than anything are full prototype grids. To attract entries, manufacturers want a category where they can develop and market their brand and technologies. To this end, careful consideration should be given to IMSA's involvement with the ACO and a universal set of LMP2 regulations. IMSA may well see an opportunity to adopt the four spec chassis as an affordable base upon which manufacturers are able to develop a unique, market appropriate package. In order to facilitate manufacturer's needs, aerodynamic and engine regulations need to be even less restrictive allowing for diverse engineering solutions. By nature, this puts IMSA teams directly at odds with the requirements to compete in ACO events.

As such, it may be time to embrace an American solution for American teams. Unless the feedback IMSA has been receiving from teams in private has been totally incongruent with sentiments shared publicly, the option to run at Le Mans is not a high priority for current stakeholders. The expense of shipping equipment and personnel across the Atlantic only to be an also-ran in a second tier category with foreign bodywork with a restricted engine is unappealing if not totally absurd. For those who do wish to race at Circuit de la Sarthe, the option always exists to lease a car or partner with an existing European outfit. This should be increasingly viable in the category's mostly spec future.

While fans, and drivers, undoubtedly enjoy seeing our favorite teams tackle the challenge of Le Mans, it does little to further the goals of the manufacturers. Manufacturers want to take overall wins in front of American consumers in vehicles that showcase their engineering expertise. Unless the new regulations do more to allow for this, we will continue to see prototype participation dwindle. For the future of prototype sportscar racing in North America, IMSA needs to resist the allure of Le Mans, distance itself from the the ACO, and look after its own domestic interests.
