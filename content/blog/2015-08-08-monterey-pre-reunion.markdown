---
author: travisba
comments: true
date: 2015-08-08 23:56:23+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/photo/monterey-pre-reunion/
slug: monterey-pre-reunion
title: 'Gallery: Monterey Pre-Reunion'
wordpress_id: 625
categories:
- Photo
---

I have an air-cooled Porsche fetish. Thankfully I was able to get my fix and see a whole host of other exciting historic racecars. Unlike next weekend's ROLEX Monterey Motorsports Reunion, crowds were small so I could get close and spend some time admiring the details.

<!-- more -->

Also check out the [video](http://www.travisbaraki.com/video/sounds-of-the-monterey-pre-reunion/) from the day. The sounds were incredible.

[juicebox gallery_id="15"]
