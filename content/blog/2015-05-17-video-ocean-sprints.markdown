---
author: travisba
comments: true
date: 2015-05-17 04:24:32+00:00
draft: true
layout: post
link: http://www.travisbaraki.com/video/video-ocean-sprints/
slug: video-ocean-sprints
title: 'Video: Ocean Sprints'
wordpress_id: 197
categories:
- Video
---

I decided to bring my camera with me to Ocean Speedway just to see if the lighting was sufficient. I learned two things:



	
  1. There is plenty of light available and my camera does a very impressive job at high ISO

	
  2. I should consider dust proofing my camera


I will definitely be back, better prepared, looking for clean sight lines.

